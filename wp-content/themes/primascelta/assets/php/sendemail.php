<?php
ob_start();
require '../../../../../wp-load.php';
// Only process POST reqeusts.
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get the form fields and remove whitespace.
    $form = [
        'name'                           => trim(urldecode($_POST["nome"])),
        'address'                        => trim(urldecode($_POST["citta"])),
        'email'                          => filter_var(trim(urldecode($_POST["email"])), FILTER_SANITIZE_EMAIL),
        'tel'                            => trim(urldecode($_POST["tel"])),
        'shop'                           => trim(urldecode($_POST["negozio"])),
        'coupon'                         => trim(urldecode($_POST["coupon"])),
        'description'                    => trim(urldecode($_POST["description"])),
        'street_number'                  => trim(urldecode($_POST["street_number"])),
        'route'                          => trim(urldecode($_POST["route"])),
        'locality'                       => trim(urldecode($_POST["locality"])),
        'administrative_area_level_1'    => trim(urldecode($_POST["administrative_area_level_1"])),
        'postal_code'                    => trim(urldecode($_POST["postal_code"])),
        'country'                        => trim(urldecode($_POST["country"])),
        'lat'                            => trim(urldecode($_POST["lat"])),
        'long'                           => trim(urldecode($_POST["long"])),
        'message'                        => trim(urldecode($_POST["message"])),
        'save_to_app'                    => trim(urldecode($_POST["save_to_app"]))
    ];

    
    
    // Check that data was sent to the mailer.
    if ( empty($form["name"]) OR empty($form["tel"]) OR !filter_var($form["email"], FILTER_VALIDATE_EMAIL)) {
        // Set a 400 (bad request) response code and exit.
        http_response_code(400);
        echo "Attenzione! Alcuni campi non sono stati compilati correttamente. Per favore ricompila il form e prova ancora.";
        exit;
    }
    
    global $wpdb; 
    $wpdb->insert('wp_registered_shops', $form);
    $shops_ID = $wpdb->insert_id;
    
    if( !is_wp_error($shops_ID) ){
        // Set a 200 (okay) response code.


        if (class_exists(\MailPoet\API\API::class)) {
            // Get MailPoet API instance
            $mailpoet_api = \MailPoet\API\API::MP('v1');
            try {
                $user_data = ['email' => $form["email"],'first_name' => $form["name"],'last_name' => $form["shop"]];
                $get_subscriber = $mailpoet_api->addSubscriber($user_data,[3],["send_confirmation_email" => false]);
            } catch (\Exception $e) {}
        }
      
        if ($form["save_to_app"] == "SI"){
            // CALL BULK SERVICES



            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://backend.primascelta.biz/api/v1/auth/bulk_register");
            curl_setopt($ch, CURLOPT_POST, 1);

            $parameters = "name=".$form["name"];
            $parameters .= "&email=".$form["email"];
            $parameters .= "&password=b5XDcnUL7nMh";
            $parameters .= "&tax_vat=tax_vat";
            $parameters .= "&tax_fiscal_code=tax_fiscal_code";
            $parameters .= "&tel=".$form["tel"];
            $parameters .= "&shop=".$form["shop"];
            $parameters .= "&shop_type_id=".$form["description"];
            $parameters .= "&coupon=".$form["coupon"];
            $parameters .= "&description=".$form["message"];
            $parameters .= "&address=".$form["address"];
            $parameters .= "&street_number=".$form["street_number"];
            $parameters .= "&route=".$form["route"];
            $parameters .= "&locality=".$form["locality"];
            $parameters .= "&administrative_area_level_1=".$form["administrative_area_level_1"];
            $parameters .= "&postal_code=".$form["postal_code"];
            $parameters .= "&country=".$form["country"];
            $parameters .= "&lat=".$form["lat"];
            $parameters .= "&long=".$form["long"];

            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

        }

        $to = "info@primascelta.biz";
        $subject = "Contatto da sito";
        $subject .= ($form["save_to_app"]) ? " con richiesta di iscrizione" : "";
        foreach($form as $key => $val){
            if ($key=="description")
                $val = getDescription($form[$key]);
            else
                $val = $form[$key];
            
            $txt .= $key . "= " . $val. "\r\n";  
        }
        $headers = "From: website@primascelta.biz" . "\r\n" .
        "Bcc: e.gennuso@gmail.com";
        
        mail($to,$subject,$txt,$headers);

        http_response_code(200);
        echo "Grazie! Il form è stato inviato correttamente.";
    } else {
        // Set a 500 (internal server error) response code.
        http_response_code(500);
        echo "Attenzione! Qualcosa è andato storto e non abbiamo potuto ricevere i dati.";
    }
} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "IT'S NOT A POST REQUEST! There was a problem with your submission, please try again.";
}
?>