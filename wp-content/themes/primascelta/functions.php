<?php

// include the class
require_once('inc/Cf7Manager.php');

// custom print_r function
function pre($text, $stop = false)
{
    echo "<pre>";
    print_r($text);
    echo "</pre>";
    if ($stop) {
        exit;
    }
}

// custom print_r function
function getDescription($n)
{
    $vett = [
        2 => "Bar",
        3 => "Bitstrot",
        4 => "Forno",
        6 => "Frutta e Verdura",
        7 => "Gelateria",
        8 => "Macelleria",
        10 => "Pasta Fresca",
        11 => "Pasticceria",
        12 => "Pescheria",
        14 => "Pizzeria",
        15 => "Prodotti bio",
        16 => "Ristorante",
        19 => "Street Food",
        1 => "Altra attività"
    ];
    return ($n == "1000") ? $vett : $vett[$n];
}

function getDelivery($n)
{
    $vett = [
        "1" => "1 km",
        "5" => "5 km",
        "10" => "10 km",
        "50" => "50 km",
        "100" => "100 km",
        "200" => "200 km",
        "5000" => "Tutta Italia"
    ];
    return ($n == "1000") ? $vett : $vett[$n];
}

/*------------------------------------*\
    MENU Functions
\*------------------------------------*/

function register_my_menus()
{
    register_nav_menus(
        array(
            'menu-top' => __('Menu di Navigazione'),
            'menu-footer-1' => __('Menu nel footer 1'),
            'menu-footer-2' => __('Menu nel footer 2'),
            'menu-footer-3' => __('Menu nel footer 3'),
            'menu-footer-4' => __('Menu nel footer 4')
        )
    );
}
add_action('init', 'register_my_menus');




// add the filter 
add_filter('wp_nav_menu', 'wp_nav_menu_remove_attributes');
function wp_nav_menu_remove_attributes($menu)
{
    return $menu = preg_replace('/ id=\"(.*)\" class=\"(.*)\"/iU', '', $menu);
}


add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
function clear_nav_menu_item_id($id, $item, $args)
{
    return "";
}

add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3);
function clear_nav_menu_item_class($classes, $item, $args)
{
    if (in_array("ctabtn", $classes))
        return array("ctabtn");
    else
        return array();
}

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

/*------------------------------------*\
    Sidebar Functions
\*------------------------------------*/

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('FOOTER 1'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-1',
        'before_widget' => '<div id="%1$s" class="%2$s=>"',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 2'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-2',
        'before_widget' => '<div id="%1$s" class="%2$s=>"',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 3'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-3',
        'before_widget' => '<div id="%1$s" class="%2$s=>"',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
    register_sidebar(array(
        'name' => __('FOOTER 4'),
        'description' => __('Description for this widget-area...'),
        'id' => 'footer-4',
        'before_widget' => '<div id="%1$s" class="%2$s=>"',
        'after_widget' => '</div>',
        'before_title' => '<span>',
        'after_title' => '</span>',
    ));
}



/*------------------------------------*\
    Shortcode section
\*------------------------------------*/

add_shortcode('faq_assistenza', 'faq_assistenza');
function faq_assistenza($args = array())
{

    $return = '<div class="boxed" id="faq">';
    $return .= '<h1 class="FAQ-heading">Domande frequenti</h1>';
    $return .= '<section class="accordion"><section class="accordion-widget model" data-css-dependency="styles/components/partials/accordion.css" data-js-dependency="scripts/components/accordion.js,scripts/components/accordion-base.js">';
    $return .= '<div class="js-Accordion" id="accordion">';
    while (have_rows('faq_repeater', 377)) : the_row();
        $return .= '<button class="null js-Accordion-title ">' . get_sub_field('domanda') . '</button>';
        $return .= '<div><div class="accordion-inside">' . get_sub_field('risposta') . '</div></div>';
    endwhile;
    $return .= '</div>';
    $return .= '</section></section>';
    $return .= '</div>';


    return $return;
}

/*------------------------------------*\
    Social Share Fix 
\*------------------------------------*/

function getSSLPage($url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: primasceltabiz_session=eyJpdiI6IlZmcWxOeEtHS3dTOGNBL1B2KzBBcmc9PSIsInZhbHVlIjoicFZaSjNPV2VNVm0vUmJYazNQODN6V01RSWJzaFdCTW5uKy8zSEtwWTJPZ1ZWK1d6OTh4UDZubUUzWHY3U3IxWDhhNlp6MFo1NVEyUmtpVkJlRWNVMjJlU2dpWWE0TE1DT1NJMHpTdTVBcEtsUzdNUWRBbDE3ZmhreDVGZXJsMUciLCJtYWMiOiI5ODdmNDFmNGU0Y2ZjNmU3YTcyYThlYjRhZTIzOGIzOTk2NTI0MjE2ZGQ5NTI0YzcyOTMyMjJmZGVkNTM1YzJlIn0%3D'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

/*------------------------------------*\
    Add options page
\*------------------------------------*/

// add acf option  page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'     => 'Agenti',
        'menu_title'    => 'Agenti',
        'menu_slug'     => 'primascelta_agenti',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));

    acf_add_options_page(array(
        'page_title'     => 'Prima Scelta',
        'menu_title'    => 'Prima Scelta',
        'menu_slug'     => 'primascelta_min_max',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
}