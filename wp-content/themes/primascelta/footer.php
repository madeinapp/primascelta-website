<div class="modal" id="modal-form" data-js-dependency="scripts/components/modal.js,scripts/components/gmaps.js">
    <a href="javascript:void(0)" class="modal-overlay closemodal" aria-label="Close"><i class="icon-plus"></i></a>
    <div class="modal-container">
        <a href="javascript:void(0);" class="closeform">
        <i class="icon-plus rotate"></i>
        </a>
        <div class="modal-title" style="text-align:center">
        <img src="https://www.primascelta.biz/wp-content/uploads/2020/04/logo-primascelta.png" style="width:20%; min-width: 160px" />
        </div>
        <div class="form-panel">
            <div class="modal-body">
                <div class="content">
                <div class="form-label" style="text-align:center"><strong>A breve saremo operativi nella tua zona: nel frattempo fatti trovare pronto!</strong></div>
                    <!-- form switch control -->
                    <form class="form-horizontal"  data-js-dependency="scripts/components/form-verifier.js">
                        <div class="form-group">
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="nome">Nome e Cognome</label>
                            <input class="form-input" type="text" id="nome" placeholder="Mario Rossi" required>
                          </div>
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="email">Email</label>
                            <input class="form-input" type="email" id="email" placeholder="m.rossi@email.it" required>
                          </div>
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="tel">Telefono</label>
                            <input class="form-input" type="tel" id="tel" placeholder="328 777 3773" required>
                          </div>
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="negozio">Nome dell' attività</label>
                            <input class="form-input" type="text" id="negozio" placeholder="Bar Imperiale" required>
                          </div>
                          <div class="col-12 col-sm-12">
                            <label class="form-label" for="citta">Indirizzo della tua attività</label>
                            <div class="form-div">Se hai più di un indirizzo nessun problema: in base all’abbonamento che sceglierai, potrai inserire fino a tre indirizzi differenti. 
Al momento però ti chiediamo di scriverne uno soltanto. Grazie</div>
                            <input class="form-input" type="text" id="citta" placeholder="Via dei Castani, 58, Roma, RM, Italia" required>
                          </div>
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="description">Che genere di attività hai?</label>
                            <select class="form-select form-input" id="description" >
                              <option value=""></option>
                              <?php 
                              foreach (getDescription(1000) as $description_key => $description_val){
                                echo '<option class="form-label" value="'.$description_key.'">'.$description_val.'</option>';
                              }
                              ?>
                            </select>
                            </div>
                          <div class="col-6 col-sm-12">
                            <label class="form-label" for="negozio">Inserisci il tuo codice coupon</label>
                            <input class="form-input" type="text" id="coupon" placeholder="ABC12"> 
                            <!--select class="form-select form-input" id="delivery">
                              <option value=""></option>
                              <?php 
                              //foreach (getDelivery(1000) as $delivery_key => $delivery_val){
                              //  echo '<option class="form-label" value="'.$delivery_key.'">'.$delivery_val.'</option>';
                              //}
                              ?>
                            </select -->
                          </div>
                          
                          
                          <div class="col-12 col-sm-12">
                            <label class="form-label" for="message">Messaggio</label>
                            <textarea class="form-textarea" cols="5" id="message" placeholder="Descrivi la tua attività e/o scrivi qui il tuo messaggio" required></textarea>
                          </div>
                          
                          
                        </div>
                        <!-- form structure -->
                        <input type="hidden" id="street_number" />
                        <input type="hidden" id="route" />
                        <input type="hidden" id="locality" />
                        <input type="hidden" id="administrative_area_level_1" />
                        <input type="hidden" id="postal_code" />
                        <input type="hidden" id="country" />
                        <input type="hidden" id="lat" />
                        <input type="hidden" id="long" />
                      </form>
                    <div class="form-group">
    
                        <label class="form-switch">
                        <input type="checkbox" id='privacy' name="privacy">
                            <i class="form-icon"></i> Ho letto e acconsento l' <a href="https://www.primascelta.biz/privacy-policy/">informativa sulla privacy</a>
                        </label>
                        <label class="form-switch">
                        <input type="checkbox" id='privacy_2' name="privacy_2">
                            <i class="form-icon"></i> Voglio che la mia attività venga trovata, pertanto vorrei inserirla gratuitamente all'interno dell'applicazione
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="submit" class="btn btn-primary input-group-btn">Invia</button>
            </div>
        </div>
        <div class="form-message" style="display: none;">
          <div class="success-msg" style="display: none;">
            <div class="form-badge">
              <img src="/wp-content/themes/primascelta/assets/success.svg" class="img-responsive" alt="">
            </div>
            <h2 id="text-success-no-app" style="display: none;">Grazie, la tua richiesta è stata inviata con successo</h2>
            <h2 id="text-success-app" style="display: none;">Grazie, la tua attività è stata aggiunta gratuitamente all'interno dell'applicazione</h2>
            <p>Ti invieremo ulteriori informazioni all'indirizzo email che hai fornito</p>
          </div>
          <div class="error-msg" style="display: none;">
            <div class="form-badge">
              <img src="/wp-content/themes/primascelta/assets/error.svg" class="img-responsive" alt="">
            </div>
            <h2>Oops! si è verificato un problema</h2>
            <p>Compila bene tutti i campi oppure riprova tra qualche secondo</p>
          </div>
        </div>
    </div>
</div>
		   
		   
		    </div> <!-- main container -->            
            <div class="footer-container" data-css-dependency="styles/components/partials/ie_fixes.css">
                <!-- <footer class="main-footer" data-js-dependency="scripts/components/dark.js"> -->
                <footer class="main-footer">
                    

                   <div class="footer__content">
					   
					
                    <div class="foter-mail">
                        <span>I prodotti migliori. Scelti per primi. Scelti per te.</span>
                    </div>



                    <div class="footer-nav">
						<div class="footer-nav__col">
							<?php dynamic_sidebar( 'footer-1' ); ?>
						</div>
						<div class="footer-nav__col">
							<?php dynamic_sidebar( 'footer-2' ); ?>
						</div>
						<div class="footer-nav__col">
							<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
						<div class="footer-nav__col">
							<?php dynamic_sidebar( 'footer-4' ); ?>
						</div>
						<div class="footer-nav__col" style="flex:0.7">
							<ul>
								<li class="nav-item">                                
									<a href="/scaricaprimascelta/" rel="nofollow"><img src="/wp-content/uploads/2020/04/appstore.png"/></a>
								</li>
								 <li class="nav-item">                                
									<a href="/scaricaprimascelta/"  rel="nofollow"><img src="/wp-content/uploads/2020/04/googleplay.png"/></a>
								</li>
							</ul>
						</div>
						
                           
                    </div>
                    
                    <div class="footer-colophon">
                        <span>Copyright © 2020 Terracciano srl. Tutti i diritti riservati. | Terracciano srl  - P. IVA IT13835881007 </span> 
                    </div>
				  </div> 
                </footer>
            </div>

        </div> <!-- main wrapper -->
        <div id="topBtn" data-css-dependency="styles/components/partials/backtotop.css" data-js-dependency="scripts/lib/smooth-scroll.polyfills.min.js, scripts/components/back-to-top.js">
  
            <a data-scroll href="#pageTop" title="Go to top">
                <i class="icon-angle-right"></i>
            </a>
        </div>

        <script src='/wp-content/themes/primascelta/scripts/main.js?ver=1.0.040'></script>

        
        <?php wp_footer(); ?>
    </body>
<script>
	
	function openMenu() {
		var menu = document.getElementById("menu-panel")
		menu.classList.add("active");
	}
	
	function closeMenu() {
		var menu = document.getElementById("menu-panel")
		menu.classList.remove("active");
	}
	
</script>	
</html>

