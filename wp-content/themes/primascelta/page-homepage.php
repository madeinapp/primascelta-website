<?php get_header(); ?>
        <div class="boxed hero-block" style="padding-bottom:0">
            <section class="heading">
                <h3 style="margin-left:10px">Se, in fatto di cibo, ti accontenti sempre.<br> Del meglio.</h3>
                <div class="grid">
                    <div class="hero-block__text col col-6 col-sm">
                        <p>
                           L'App per chi vende alimenti di prima qualità 
ed intende migliorare i propri affari. Non solo on-line.                         
                        </p>
                        <div class="bigcta">
                            <a href="/scaricaprimascelta/">PROVALA GRATIS</a>
                        </div>
                    </div>
                    <div class="hero-block__image col col-8 col-sm" style="margin-left:auto">
                        <div class="hero-image">
                            <img src="/wp-content/uploads/2020/04/hero-image.png" class="img-responsive" alt="hero Primascelta">
                        </div>
                    </div>
                </div>
            </section>
        </div>
		<div class="fullwidth categories">
			<section class="boxed heading">
				<h3 class="align-right">Se credi che nella vita si possa sempre migliorare.</h3>
				<!--p class="description align-right">
					Pensata per te che hai già un consolidato gruppo di clienti che fanno ordini al telefono o WhatsApp.
				</p -->
				<div>
					<div class="flex categories__child">
						<h2>Bar<br>Bitstrot<br>Forni<br>Frutta e Verdura<br>Gelaterie<br>Macellerie</h2>
						<div class="flex">

							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/FRUTTA.png"/>
							</div>
							<!--div class="categories__image">
								<img src="wp-content/uploads/2020/04/VERDURA.png"/>
							</div -->
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/PESCI.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/CARNI.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/05/COLAZIONE.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/05/PORZIONE.png"/>
							</div>
						</div>
						
					</div>
					<div class="flex categories__child">
						<div class="flex">
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/PANE.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/DOLCI.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/GELATI.png"/>
							</div>
							<div class="categories__image">
								<img src="wp-content/uploads/2020/04/PASTA.png"/>
							</div>
							
							<div class="categories__image">
								<img src="wp-content/uploads/2020/05/PIZZA.png"/>
							</div>
						</div>
						
						<h2>Pasta fresca<br>Pasticcerie<br>Pescherie<br>Pizzerie<br>Prodotti Bio<br>Ristoranti<br>Street Food</h2>
					</div>
				</div>
			</section>
		</div>

        <div class="boxed steps fullwidth">
            <section class="heading">
                <h3>Le tue prenotazioni aumenteranno. I tempi per gestirle no.  </h3>
                <div class="grid">
                    <div>
                        <p>
                           Questo non è un e-commerce e tu continuerai ad incassare i tuoi soldi come preferisci fare.
Con più ordini, gestiti meglio ed evasi in meno tempo. Ma sopratutto con clienti ancora più felici.
Tutto si svolgerà in quattro semplici passaggi.                     
                        </p>
                    </div>
                    <div class="flex" style="flex-wrap: wrap">
						<div class="col-3 step" style="max-width: 50%">
							<img src="/wp-content/uploads/2020/05/step1.png" alt="step1 Primascelta"/>
							<span>Prova gratis l'app Primascelta</span>
						</div>
						<div class="col-3 step" style="max-width: 50%">
							<img src="/wp-content/uploads/2020/05/step2.png" alt="step2 Primascelta"/>
							<span>Crea il tuo catalogo</span>
						</div>
						<div class="col-3 step" style="max-width: 50%">
							<img src="/wp-content/uploads/2020/05/step3.png" alt="step3 Primascelta"/>
							<span>Invita clienti vecchi e nuovi</span>
						</div>
						<div class="col-3 step" style="max-width: 50%">
							<img src="/wp-content/uploads/2020/05/step4.png" alt="step4 Primascelta"/>
							<span>Ricevi subito i primi ordini</span>
						</div>
						
                    </div>
                </div>
            </section>
        </div>


<!--
        <div class="fullwidth bg-palette-3">
            <section class="boxed about" data-css-dependency="styles/components/partials/card.css">
                <h2><span>Come funziona?</span> Tre semplici Passaggi:</h2>
                <div class="grid">
                    <div class="col col-4 col-sm">
                        <div class="card">
                            <div class="card-img">
                                <img src="/wp-content/themes/primascelta/assets/1.svg" class="img-responsive round" alt="">
                            </div>
                            <h3>Attiva <span>e-commerce</span> gratuito</h3>
                        </div>
                    </div>
                    <div class="col col-4 col-sm">
                        <div class="card">
                            <div class="card-img">
                                <img src="/wp-content/themes/primascelta/assets/2.svg" class="img-responsive round" alt="">
                            </div>
                            <h3><span>Contatta</span> i tuoi clienti abituali</h3>
                        </div>
                    </div>
                    <div class="col col-4 col-sm">
                        <div class="card">
                            <div class="card-img">
                                <img src="/wp-content/themes/primascelta/assets/3.svg" class="img-responsive round" alt="">
                            </div>
                            <h3>Trova e <span>coinvolgi</span> nuovi clienti</h3>
                        </div>
                    </div>
                </div>
            </section>
        </div>
-->




<?php get_footer(); ?>
