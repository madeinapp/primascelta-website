<?php

/**
 * CF7Manager
 *
 * @author  roberto di marco
 * @version 1.0
 */



if ( ! class_exists( 'CF7Manager' ) ) {

	class CF7Manager {






		function __construct() {        }

		function init() {
			add_filter( 'wp_head', array( $this, 'primascelta_init_css' ) );
			add_filter( 'wp_footer', array( $this, 'primascelta_init_js' ) );
			add_filter( 'wpcf7_form_tag', array( $this, 'primascelta_dynamic_fields' ) );
			add_filter( 'wpcf7_before_send_mail', array( $this, 'wpcf7_before_send_mail_function' ), 10, 3 );
			add_filter( 'wpcf7_validate_text', array( $this, 'wpharvest_custom_validation_text' ), 999, 2 );
			add_filter( 'wpcf7_messages', array( $this, 'mywpcf7_text_messages' ) );
			add_filter( 'wpcf7_field_group_add_button_atts', array( $this, 'customize_add_button_atts' ) );
			add_filter( 'wpcf7_field_group_remove_button_atts', array( $this, 'customize_remove_button_atts' ) );

			add_filter( 'wpcf7_validate', array( $this, 'custom_validation' ), 10, 2 );

			// creo utenti dal front-end
			// add_action('wpcf7_before_send_mail', [$this, 'all_create_user_from_registration'], 1, 2);
		}

		function getMinProducts() {
			return get_field( 'primascelta_min', 'options' );
		}

		function getMaxProducts() {
			return get_field( 'primascelta_max', 'options' );
		}

		function getProducts( $array ) {
			$count = 0;
			foreach ( $array as $item ) {
				// if item name contains nomeProdotto
				if ( strpos( $item['name'], 'nomeProdotto' ) !== false && $_POST[ $item['name'] ] != '' ) {
					$count++;
				}
			}
			return $count;
		}
		function custom_validation( $result, $tags ) {
			$valid = false; // here you can do your API call to calculate the validation

			$count = $this->getProducts( $tags );
			// if is between 10 and 15 products
			if ( $count >= $this->getMinProducts() && $count <= $this->getMaxProducts() ) {
				$valid = true;
			}

			if ( ! $valid ) {

				$result->offsetSet(
					'reason',
					array( 'privacy-check' => 'Devi inserire minimo ' . $this->getMinProducts() . ' prodotti. Puoi inserirne fino a ' . $this->getMaxProducts() . '.' )
				);

			}

			return $result;
		}

		function mywpcf7_text_messages( $messages ) {
			return array_merge(
				$messages,
				array(
					'invalid_product' => array(
						'description' => __( 'Prodotto is invalid', 'contact-form-7' ),
						'default'     => __( 'Questo campo non può essere vuoto.', 'contact-form-7' ),
					),
				)
			);
		}
		// In your theme's functions.php
		function customize_add_button_atts( $attributes ) {
			return array_merge(
				$attributes,
				array(
					'text' => '+ Aggiungi un altro prodotto',
				)
			);
		}

		function customize_remove_button_atts( $attributes ) {
			return array_merge(
				$attributes,
				array(
					'text' => '- Rimuovi questo prodotto',
				)
			);
		}

		function wpharvest_custom_validation_text( $result, $tag ) {
			// check if this is our phone field, and return the $result unchanged if it isn't:
			$tag = new WPCF7_Shortcode( $tag );
			// get tag value
			$name = $tag->name;
			// if tag name contain prodotto and explode  name is number between 1 and 10
			if ( strpos( strtolower( $tag->name ), 'prodotto' ) !== false ) {
				// if tag name not contain descrizione
				if ( strpos( strtolower( $tag->name ), 'descrizione' ) === false ) {
					$value  = isset( $_POST[ $name ] ) ? trim( wp_unslash( strtr( (string) $_POST[ $name ], "\n", ' ' ) ) ) : '';
					$name   = explode( '_', $tag->name );
					$number = $name[2];
					$number = intval( $number );
					if ( $number <= $this->getMinProducts() && $value == '' ) {
						$result->invalidate( $tag, wpcf7_get_message( 'invalid_product' ) );
					}
				}
			}
			return $result;
		}
		function getAgenti() {
			return get_field( 'agenti_gruppo', 'options' );
		}

		function getAgente( $codiceAgente ) {
			$agenti = $this->getAgenti();

			foreach ( $agenti as $agente ) {
				if ( $agente['codice'] == $codiceAgente ) {
					return $agente;
				}
			}
			return false;
		}

		function getAgenteField( $codiceAgente, $field ) {
			if ( $this->getAgente( $codiceAgente ) ) {
				return $this->getAgente( $codiceAgente )[ $field ];
			}
			return '';
		}

		function primascelta_init_js() {                    ?>
			<script>
				var itemFirst = document.querySelector('.wpcf7-list-item.first span');
				// replace NO with strong
				itemFirst.innerHTML = itemFirst.innerHTML.replace('NO', '<strong>NO</strong>');

				var itemLast = document.querySelector('.wpcf7-list-item.last span');
				// replace SI with strong
				itemLast.innerHTML = itemLast.innerHTML.replace('SI', '<strong>SI</strong>');
				// Get the element
				var elem = document.querySelector('.product');

				var group_add = document.querySelector('.wpcf7-field-group-add');
				// clone the element 14 times


				jQuery( '.wpcf7-field-groups' ).on( 'wpcf7-field-groups/change', function() {
					var $groups = jQuery( this ).find( '.wpcf7-field-group' );

					// write the number of the group
					$groups.each( function( index ) {
						// if index is less than 14
						if ( index <= 14 ) {
							// get the last element with class .wpcf7-list-item
							var last_span = jQuery( this ).find( '.product_number' )[0];
							// get the last element with class .wpcf7-list-item
							last_span.innerHTML = index + 1;
						} else {
							// remove the element
							jQuery( this ).remove();
							jQuery('.wpcf7-response-output').show().css('border-color','red').html('Il numero massimo di prodotti da inserire è 15');

							setTimeout(function(){
								jQuery('.wpcf7-response-output').hide().html('').css('border-color','');
							}, 3000);
						}
					
					} );
					
				} ).trigger( 'wpcf7-field-groups/change' );
				
			</script>

			<?php
		}
		function primascelta_init_css() {
			?>

			<style>
				#contact-form-1234 .form-label {
					display: inline-block;
				}

				#contact-form-1234 .form-group {
					display: block;
				}

				#contact-form-1234 label {
					min-width: 200px;
				}

				#contact-form-1234 input {
					border-radius: 30px !important;
				}

				#contact-form-1234 input[type="radio"] {
					width: auto;
				}

				#contact-form-1234 .product {
					padding: 10px;
					margin: 10px 0;
					border-radius: 5px;
					box-shadow: 3px 3px 5px 0px #eee;
					max-width: 100%;
				}

				@media screen and (min-width:1024px) {
					#contact-form-1234 .product .form-inline {
						display: flex;
					}
				}


				.m-0 {
					margin: 0 !important;
				}

				.m-0 img {
					width: 100% !important;
				}

				#contact-form-1234 input[name="privacy-check"] {
					width: auto;
				}

				#contact-form-1234 a {
					text-decoration: underline;
					font-weight: bold;
				}

				#contact-form-1234 input:disabled {
					opacity: .5;
				}

				#contact-form-1234 .wpcf7-field-group-remove  {
					margin-right: 20px;
				}
				#contact-form-1234 .required  {
					color: red;
				}
				@media screen and (max-width:768px) {
					#contact-form-1234 .wpcf7-field-group-remove  {
						margin-right: 0;
						margin-top: 20px;
						margin-bottom: 20px;
					}
				}
			</style>

			<?php
		}

		function getAgents() {
			$curl = curl_init();

			curl_setopt_array(
				$curl,
				array(
					CURLOPT_URL            => 'https://backend.primascelta.local/api/v1/agents',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING       => '',
					CURLOPT_MAXREDIRS      => 10,
					CURLOPT_TIMEOUT        => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST  => 'GET',
					CURLOPT_SSL_VERIFYPEER => false,
					CURLOPT_SSL_VERIFYHOST => false,
					CURLOPT_HTTPHEADER     => array(
						'Authorization: Bearer qnecYmKikABMrwbdURpIt9QX2nOgvyaX',
						'Cookie: primasceltabiz_session=eyJpdiI6ImZmSzNyRW9OUVM0ZitMTmNVQi9CYVE9PSIsInZhbHVlIjoiN05MQ0RDT0Ria0dJanpPVHQzVmN6S0JXclh0RUo4TnVIWnU2RkE1ekxKYnQvRnc5M0diK1RBdG03ZGFYTTJvUHlVdktmZm5pS1lWVmJ2WERYNE1vSmN3aGcyUm1EVUl3MDdKcmw1dXF2UHArR0NtRk14U1dveW4zS2ROa2pqVkMiLCJtYWMiOiIzNDljMmIzMzNlZGFlOGIxZTcxNjQzNGQxYjlkYTdlMDE4MmZjZGVlOTc1M2I2N2I5OWE4ZjMxNzY1Y2U1NTk5In0%3D',
					),
				)
			);

			$response = curl_exec( $curl );

			curl_close( $curl );

			return ( $response ) ? json_decode( $response, true )['data'] : false;
		}

		function primascelta_dynamic_fields( $tag ) {

			switch ( $tag['name'] ) {

				case 'agenti_form':
					break;

				case 'codiceAgente':
					$tag['values'][] = ( isset( $_GET['codiceAgente'] ) ? $_GET['codiceAgente'] : '' );

					break;
				case 'nomeAgente':
					$tag['values'][] = ( isset( $_GET['codiceAgente'] ) ? $this->getAgenteField( $_GET['codiceAgente'], 'nome' ) : '' );
					break;
				case 'emailAgente':
					$tag['values'][] = ( isset( $_GET['codiceAgente'] ) ? $this->getAgenteField( $_GET['codiceAgente'], 'email' ) : '' );
					break;
				default:
					break;
			}

			return $tag;
		}

		function wpcf7_before_send_mail_function( $contact_form, $abort, $submission ) {

			// get the submitted data from the form
			$posted_data = $submission->get_posted_data();
			$properties  = $contact_form->get_properties();

			// check if agenti_form and emailAgente is set
			if ( isset( $posted_data['agenti_form'] ) && isset( $posted_data['emailAgente'] ) ) {

				$properties['mail']['subject']   = 'Nuova attività da censire agente' . ' - ' . $posted_data['nomeAgente'] . ' - ' . $posted_data['codiceAgente'];
				$properties['mail']['recipient'] = $posted_data['emailAgente'];

				// separate the fields
				$properties['mail']['body'] = 'Nuova scheda ' . "\n\n\n\n";

				// append posted data to mail body
				foreach ( $posted_data as $key => $value ) {

					if ( $key === 'rider' ) {
						$properties['mail']['body'] .= 'rider: ' . $value[0] . "\n";
					}

					if ( strpos( $key, 'nomeProdotto-' ) !== false ) {
						// explode the key to get the product name
						$exploded_key = explode( '-', $key );

						$properties['mail']['body'] .= '<br><b>Prodotto ' . $exploded_key[1] . '' . "</b>\n";
					}
					if ( strpos( $key, 'nomeProdotto-' ) !== false ) {
						$key = 'Nome';
					}
					if ( strpos( $key, 'prezzoProdotto-' ) !== false ) {
						$key = 'Prezzo';
					}
					if ( strpos( $key, 'descrizioneProdotto-' ) !== false ) {
						$key = 'Descrizione';
					}

					$properties['mail']['body'] .= $key . ': ' . $value . "\n";
				}
				$contact_form->set_properties( $properties );
			}

			return $contact_form;
		}
	} //

	$cf7 = new CF7Manager();
	$cf7->init();
}

// base class with member properties and methods
