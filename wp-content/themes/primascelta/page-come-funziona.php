<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
remove_filter( 'the_content', 'wpautop' );
?>
	<section class="faq hero-section">
		<div class="boxed flex">
			<div class="faq__bgimage faq__bgimage--1">
				<img src="/wp-content/uploads/2020/04/FAQ-bg-sx.png"/>
			</div>
			<div class="video-container">
				<div>
				<iframe title="vimeo-player" src="https://player.vimeo.com/video/411959019" width="740" height="416.25" frameborder="0" allowfullscreen ></iframe>
				</div>
			</div>
			<div class="faq__bgimage faq__bgimage--2">
				<img src="/wp-content/uploads/2020/04/FAQ-bg-dx.png"/>
			</div>
		</div>
	</section>

        <div class="boxed" id="faq">
			<h1 class="FAQ-heading">
            <?php the_title(); ?>
			</h1>
            <section class="accordion">
               
<section class="accordion-widget model"
         data-css-dependency="styles/components/partials/accordion.css" 
         data-js-dependency="scripts/components/accordion.js,scripts/components/accordion-base.js">
    <div class="js-Accordion" id="accordion">
    <?php echo str_replace(["<p>","</p>"],"",get_the_content()); ?>
            </div>

</section>

            </section>
        </div>




                        <?php endwhile; endif; ?>
                   
<?php get_footer(); ?>
