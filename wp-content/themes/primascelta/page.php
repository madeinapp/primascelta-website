<?php get_header(); 
$video = get_field("video");
if ($video != "") :?>
        <section class="faq hero-section">
            <div class="boxed flex">
                <div class="faq__bgimage faq__bgimage--1">
                    <img src="/wp-content/uploads/2020/04/FAQ-bg-sx.png"/>
                </div>
                <div class="video-container">
                    <div>
                    <iframe title="vimeo-player" src="<?php echo get_field("video");?>" width="740" height="416.25" frameborder="0" allowfullscreen ></iframe>
                    </div>
                </div>
                <div class="faq__bgimage faq__bgimage--2">
                    <img src="/wp-content/uploads/2020/04/FAQ-bg-dx.png"/>
                </div>
            </div>
        </section>
<?php endif;?>
        <div class="boxed hero-block " style="padding-bottom:0">
            <section  <?php echo ($video == "") ? 'class="heading pages " ' : ''?> >
            
                <h1 <?php echo ($video != "") ? 'class="FAQ-heading" style="margin-bottom:40px" ' : '' ?> >
                <?php the_title(); ?>
                </h1>
                <div class="grid">
                    <div class=" col col-12 col-sm">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php if ( has_post_thumbnail() ) : ?>
                        <a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false ); echo esc_url( $src[0] ); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                   
                </div>
            </section>
        </div>
<?php get_footer(); ?>
