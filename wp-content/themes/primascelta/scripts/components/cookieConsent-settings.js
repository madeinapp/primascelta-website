window.cookieconsent.initialise({
    "palette": {
        "popup": {
        "background": "#13bf9e"
        },
        "button": {
        "background": "#f7b321"
        }
    },
    "theme": "classic",
    "content": {
        "message": "Questo sito web utilizza cookie per analizzare il nostro traffico. Maggiori informazioni puoi trovarle cliccando su \"maggiori informazioni\". Premi il tasto accetta per acconsentire l'uso dei cookie.",
        "dismiss": "Accetta",
        "link": "Maggiori informazioni",
        "href": "https://www.primascelta.biz/cookie-policy/"
    }
});