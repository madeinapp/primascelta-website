
(function() {
    var cta_buttons = document.querySelectorAll('.ctabtn');
    var modal_panel = document.querySelector('.modal');

    function resetForm(){
        var form_panel = document.querySelector('.form-panel');
        var form_message = document.querySelector('.form-message');
        var ok = document.querySelector('.success-msg'), ko = document.querySelector('.error-msg');
        form_panel.style.display = 'block';
       
        ok.style.display = 'none';
        ko.style.display = 'none';
        form_message.style.display = 'none';
    }

    for(var i=0; i<cta_buttons.length; i++)
        cta_buttons[i].addEventListener('click' , function() {
            /*
            resetForm();
            modal_panel.classList.add('active');
            document.body.classList.add('modalIsOpen');
            */
           self.location.href="https://www.primascelta.biz/scaricaprimascelta/";
        })

    var closemodal = document.querySelector('.closemodal');
    var closeform = document.querySelector('.closeform');
    var modal_panel = document.querySelector('.modal');

    closemodal.addEventListener('click' , function() {
        modal_panel.classList.remove('active');
        document.body.classList.remove('modalIsOpen');
    })

    closeform.addEventListener('click' , function() {
      modal_panel.classList.remove('active');
      document.body.classList.remove('modalIsOpen');
    })
  })();


  