
(function() {
    var cta_button = document.querySelector('#open_info_modal');
    var cta_button_mb = document.querySelector('#open_info_modal_mb');
    var modal_panel = document.querySelector('#modal-info');
    var closemodal = document.querySelector('.closemodalinfo');
    var continue_close = document.querySelector('#continue_close');

    cta_button.addEventListener('click' , function() {
        modal_panel.classList.add('active');
    })
    cta_button_mb.addEventListener('click' , function() {
        modal_panel.classList.add('active');
    })
    closemodal.addEventListener('click' , function() {
      modal_panel.classList.remove('active');
    })
    continue_close.addEventListener('click' , function() {
        modal_panel.classList.remove('active');
    })

  })();