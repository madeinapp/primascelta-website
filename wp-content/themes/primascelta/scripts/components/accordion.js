/**
* @fileOverview
* @author Zoltan Toth
* @version 1.1.0
*/

/**
* @description
* Vanilla JavaScript Accordion
*
* @class
* @param {(string|Object)} options.element - HTML id of the accordion container
*         or the DOM element.
* @param {number|Array} [options.openTab=1] - Start the accordion with this item opened.
* @param {boolean} [options.oneOpen=false] - Only one tab can be opened at a time.
*
*/
var Accordion = function (options) {
    var element = typeof options.element === 'string' ?
        document.getElementById(options.element) : options.element,
        openTab = options.openTab,
        oneOpen = options.oneOpen || false,

        titleClass = 'js-Accordion-title',
        contentClass = 'js-Accordion-content';

    render();

    /**
     * Initial rendering of the accordion.
     */
    function render() {
        var buttons = element.querySelectorAll('button');

        for(var i =0; i<buttons.length;i++) { 
            var btn_classes = buttons[i].getAttribute('class');
            var box_classes = buttons[i].nextElementSibling.getAttribute('class');
            buttons[i].setAttribute('class', btn_classes +' '+ titleClass);
            buttons[i].nextElementSibling.setAttribute('class', box_classes +' '+ contentClass);
        }

        // accordion starts with all tabs closed
        closeAll();

        // attach only one click listener
        element.addEventListener('click', onClick);

        
        // sets the open tab - if defined
        if (typeof openTab == "object") openTab.forEach(element => open(element));
        if (openTab) open(openTab);        
        
    }

    /**
     * Handles clicks on the accordion.
     *
     * @param {object} e - Element the click occured on.
     */
    function onClick(e) {
        var classes = e.target.getAttribute('class');
        if (classes == null) return;
        if (classes.indexOf(titleClass) === -1) return;

        toggle(e.target, e.target.nextElementSibling);
    }

    /**
     * Closes all accordion tabs.
     */
    function closeAll() {
        var btns = element.querySelectorAll('.' + titleClass);
        var boxes = element.querySelectorAll('.' + contentClass);
        
        for(var i=0; i< btns.length; i++ ) 
            _close(btns[i], boxes[i]);
    }

    /**
     * Toggles corresponding tab for each title clicked.
     *
     * @param {object} btn - The button clicked
     * @param {object} el - The content tab to show or hide.
     */
    function toggle(btn, el) {

        var classes = btn.getAttribute('class');
        var isopen = (classes).indexOf('open') > -1;

        if(isopen) _close(btn, el);
        else _open(btn, el);
    }

    function _close(btn, box) {
        var classes = btn.getAttribute('class');
        box.style.height = 0;
        btn.setAttribute('class', classes.replace('open',''));
    }

    function _open(btn, box) {
        
        if(oneOpen) closeAll();
        
        if( btn ){
            var classes = btn.getAttribute('class');
            var height = box.scrollHeight;
            
            box.style.height = height + 'px';
            btn.setAttribute('class', classes+' open');
        }
    }

    /**
     * Returns the corresponding accordion content element by index.
     *
     * @param {number} n - Index of tab to return
     */
    function getTarget(n) {
        return element.querySelectorAll('.' + contentClass)[n - 1];
    }

    function getBtn(n) {
        return element.querySelectorAll('.' + titleClass)[n - 1];
    }

    /**
     * Opens a tab by index.
     *
     * @param {number} n - Index of tab to open.
     *
     * @public
     */
    function open(n) {
        _open(getBtn(n), getTarget(n));
    }

    /**
     * Closes a tab by index.
     *
     * @param {number} n - Index of tab to close.
     *
     * @public
     */
    function close(n) {
        _close(getBtn(n), getTarget(n));
    }

    /**
     * Destroys the accordion.
     *
     * @public
     */
    function destroy() {
        element.removeEventListener('click', onClick);
    }

    return {
        open: open,
        close: close,
        destroy: destroy
    };
};