window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 120 || document.documentElement.scrollTop > 120) {
        document.getElementById("topBtn").style.display = "block";
        document.querySelector(".navbar").classList.add("scrolled");
        //document.querySelector(".search-button").classList.add("visible");
        //document.querySelector(".menu").classList.add("visible");
    } else {
        document.getElementById("topBtn").style.display = "none";
        document.querySelector(".navbar").classList.remove("scrolled");
        //document.querySelector(".search-button").classList.remove("visible");
        //document.querySelector(".menu").classList.remove("visible");
    }
}

var scroll = new SmoothScroll('a[href*="#"]', {
	speed: 800,
    speedAsDuration: true,
    offset: 80
});
 





