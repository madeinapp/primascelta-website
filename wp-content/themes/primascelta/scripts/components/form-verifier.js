(function(){
    /** ==== contact form ====  */
     var form = document.querySelector('.form-horizontal');
     var privacy = document.getElementById('privacy');
     var privacy_2 = document.getElementById('privacy_2');
     var submit  = document.getElementById('submit');
     var email   = document.getElementById('email');
     var citta = document.getElementById('citta');
     var nome = document.getElementById('nome');
     var negozio = document.getElementById('negozio');
     var description = document.getElementById('description');
     var coupon = document.getElementById('coupon');
     var lat = document.getElementById('lat');
     var save_to_app = "NO";
     var text_success_noapp = document.getElementById('text-success-no-app');
     var text_success_app = document.getElementById('text-success-app');
 
     function isEmailValid(email) {
         return (/^\w+([\.-]?\w+)*@\w+([\.-]?w+)*(\.\w{2,3})+$/.test(email));
     }
 
     function updateElements(){
         var nodes = [].map.call(document.querySelectorAll('input'), function(item) { return item}).filter(function(item){ return item.id!= 'privacy'});      
         var elements = {};
         for(var i=0; i<nodes.length; i++) elements[nodes[i].id] = nodes[i].value;
         elements.coupon = coupon.value;
         elements.description = description.value;
         if (privacy_2.checked == true) {
             save_to_app = "SI"; 
             text_success_noapp.setAttribute("style", "display:none");
             text_success_app.removeAttribute("style");
         }else{
             save_to_app = "NO";
             text_success_app.setAttribute("style", "display:none");
             text_success_noapp.removeAttribute("style");
         } 
         //console.log("privacy_2.checked",privacy_2.checked);
         //console.log("save_to_app",save_to_app.value);
         
         return elements;
     }
 
     function submitDisabled(){
         return !(privacy.checked  && isEmailValid(elements.email) && elements.lat != '' && elements.citta != '' && elements.nome != '' && elements.negozio != '' && elements.description != '' );
     }
 
     var elements = updateElements();
 
     submit.disabled = submitDisabled();
 
     privacy.addEventListener('change', function(e) { 
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 
     privacy_2.addEventListener('change', function(e) { 
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 
     email.addEventListener('change', function(e) {
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 
     lat.addEventListener('change', function(e) {
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
     citta.addEventListener('change',function(){
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
     nome.addEventListener('change',function(){
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 
     negozio.addEventListener('change',function(){
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 
     description.addEventListener('change',function(){
         elements = updateElements();
         submit.disabled = submitDisabled();
     });
 

     
     email.addEventListener('input', function(e) { email.style.color = isEmailValid(e.target.value) ? 'black' : 'red'; });
 
     function show(OK){
         var form_panel = document.querySelector('.form-panel');
         var form_message = document.querySelector('.form-message');
         var to_show = (OK) ? document.querySelector('.success-msg') : document.querySelector('.error-msg');
         var to_hide = (!OK) ? document.querySelector('.success-msg') : document.querySelector('.error-msg');
         form_panel.style.display = 'none';
         form_message.style.display = 'block';
         to_show.style.display = 'block';
         to_hide.style.display = 'none';
     }
 
     submit.addEventListener('click', function(e){
 
         e.preventDefault();
         submit.innerText="invio modulo in corso ...";
         submit.disabled = true;
         var xhr = new XMLHttpRequest();
         
         xhr.open("POST", "https://www.primascelta.biz/wp-content/themes/primascelta/assets/php/sendemail.php", true);
         
         //Send the proper header information along with the request
         xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
         
         xhr.onreadystatechange = function() { // Call a function when the state changes.
             if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                 show(true);
                 submit.innerText="Modulo inviato";
                 submit.disabled = true;
             } else {
                 show(false);
                 submit.innerText="Invia";
                 submit.disabled = false;
             }
            
         }
         var keys = Object.keys(elements);
         var couples  = []; for(var i=0; i<keys.length; i++) couples.push(keys[i]+'='+encodeURI(elements[keys[i]]));
         var body     = couples.join('&')+"&save_to_app="+save_to_app;
         //console.log("keys",keys);
         //console.log("body",body);
         xhr.send(body);
     });
 
 }());