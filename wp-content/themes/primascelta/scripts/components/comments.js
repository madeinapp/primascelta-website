(function() {
    var toggle_comments = document.querySelector('#toggle-content-id1');
    var comments_panel = document.querySelector('.article-footer');
    var icon_rotate = document.querySelector('.icon-angle-down');

    toggle_comments.addEventListener('click' , function() {
        comments_panel.classList.toggle('open');
        icon_rotate.classList.toggle('rotate');
    })
})(); 