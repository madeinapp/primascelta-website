"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Page
 * 
 * This object is responsible for
 * 
 * - retireving current page's properties, like type, title,
 * - related posts,
 * - giving the correct way to retrieve data in the markup for each type
 * 
 * @param    {string} dataSelector selector of the element which contains data, default [data-container]
 * @param    {object} props {
 *      @property {string} type - the type of container
 *      @property {string} container - the main container selector (CSS-like notation eg. '.container') 
 *      @property {boolean} infinite - true if new pages have to be automatically loaded and appended to this page
 *      @property {number} maxLoads  - max number of automatic "infinite" loadings at the end of the page
 * }  
 */
var Page = function Page() {
  var dataSelector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '[data-container]';

  _classCallCheck(this, Page);

  try {
    this.props = JSON.parse(document.querySelector(dataSelector).dataset.container);
    /*** Add current page in first position  ****/

    var current_content = {
      title: document.title,
      url: window.location.protocol + '//' + window.location.hostname + window.location.pathname
    };
    this.props.contents.unshift(current_content);
    /*console.log('dataSelector',dataSelector);
    console.log(document.querySelector(dataSelector));
    console.log('dataset',document.querySelector(dataSelector).dataset);
    */
  } catch (e) {
    // if the page doesn't provide a dataset 
    // props are set as follows
    this.props = {
      error: "".concat(e.message, " or selector ").concat(dataSelector, " not found "),
      type: 'single',
      title: '',
      paged: false,
      contents: [{
        title: document.title,
        url: window.location.href
      }],
      infinite: false,
      // [!] It's the content wrapper, it wraps each content loaded (and the initial content as well)
      ajaxSelector: '',
      appendToRequest: '?is_ajax_request'
    };
  }
}; // export default Page;

/*

OLD DATA

 for(var i=0; i<contentElements.length; i++){    
            this.contents.push({
                url   : contentElements[i].dataset.url,
                tracking_url : contentElements[i].dataset.trackUrl,
                title : contentElements[i].dataset.title
            })
        }
    moreContents = callback => {

        
        
        var text_loader = document.querySelector('#loadMore .text-loader');
        var loader_icon = document.querySelector('#loadMore .loader-ellips');
        var btn_more_articles = document.querySelector('#load-more-button');

        if(text_loader) text_loader.classList.add('hidden');
        
        if(loader_icon) loader_icon.classList.add('hidden');
        
        if(btn_more_articles){
            btn_more_articles.classList.remove('hidden');
            btn_more_articles.onclick = function() {
                btn_more_articles.classList.add('hidden');
                callback.call();
            };        
        }
    }

    loading = title => {
        
        var text_loader = document.querySelector('#loadMore .text-loader');
        var text_loader_title = document.querySelector('#loadMore .text-loader span');
        var loader_icon = document.querySelector('#loadMore .loader-ellips');

        if(text_loader_title)   text_loader_title.textContent = title;            
        if(text_loader)         text_loader.classList.remove('hidden');
        if(loader_icon)         loader_icon.classList.remove('hidden');
    }

    loaded = () => {        
        var text_loader = document.querySelector('#loadMore .text-loader');
        var loader_icon = document.querySelector('#loadMore .loader-ellips');        

        if(text_loader) text_loader.classList.add('hidden');
        if(loader_icon) loader_icon.classList.add('hidden');
    }

    setContents = () => {
    
        contentElements = document.querySelectorAll('#blz-related span');
        
        for(var i=0; i<contentElements.length; i++){    
            this.contents.push({
                url   : contentElements[i].dataset.url,
                tracking_url : contentElements[i].dataset.trackUrl,
                title : contentElements[i].dataset.title
            })
        }
    }
}

//Single.prototype = Object.create(Container.prototype);

class Page extends Container {
    constructor(content){
        super(content);
        this.paged = false;        
    }

    setContents = () => {}
}

Page.prototype = Object.create(Container.prototype);

class Home extends Container {
    constructor(content){
        super(content);
        this.paged = false;
    }    

    moreContents = callback => {        
        var loader_icon = document.querySelector('#loadMore .loader-ellips');
        var btn_more_articles = document.querySelector('#load-more-button');
        if(loader_icon) loader_icon.classList.add('hidden');
        if( btn_more_articles ){
            btn_more_articles.classList.remove('hidden');
            btn_more_articles.onclick = function() {
                btn_more_articles.classList.add('hidden');
                callback.call();
            };        
        }
    }

    loading = title => {        
        var loader_icon = document.querySelector('#loadMore .loader-ellips');
        if(loader_icon) loader_icon.classList.remove('hidden');
    }

    loaded = () => {        
        var loader_icon = document.querySelector('#loadMore .loader-ellips');
        if(loader_icon) loader_icon.classList.add('hidden');
    }

    setContents = () => {        
        var url_split = window.location.href.split('/');
        var pag = true;
        for(var i=0;i<url_split.length;i++) if(url_split[i] == 'page') pag = false;
        
        var number_of_pages = (pag)?3:0; // forces three scrolls on the home page

        for(var i=2; i<=number_of_pages; i++){
    
            this.contents.push({
                url   : window.location.href.split('/')[0]+'//'+window.location.href.split('/')[2]+'/page/'+i+'/',
                tracking_url : 'telefonino - home',
                title : 'telefonino - dimensione mobile'
            })
        }
    }
}
*/
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class DynamicDepsLoader
 *  
 * @description
 *  Responsibilities:
 *  > looks at the DOM and find the component's dependencies 
 *  > loads the dependencies and caches them in the LocalStorage
 *
 * 
 * HOW IT WORKS
 * ------------
 * 
 * The dependecies are inserted into the component's markup
 * as data- attribute 
 * 
 * single JS dependecy:
 * <span data-js-dependency="file.min.js"></span>
 * 
 * double (and nested) JS dependencies:
 * in this case the second js is loaded and launched after the 
 * execution of the first one
 * <span data-js-dependency="mylib.min.js, file_that_uses_mylib.min.js"></span>
 * 
 * single CSS dependency:
 * <span data-css-dependency='style.min.css'></span>
 * 
 * multiple CSS dependencies: in this case add more than one dependency
 * in priority order. ex:
 * <span data-css-dependency='first.css'></span>
 * <span data-css-dependency='second.css'></span>  
 * 
 * NOTE: we could add other feature like cache expiration times
 * 
 * @param {dictionary} args { jsdir, cssdir, js_dep_selector, css_dep_selector }
 */
var DynamicDepsLoader = /*#__PURE__*/function () {
  function DynamicDepsLoader() {
    var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
      jsdir: '/js/components',
      cssdir: '/css/components/',
      js_dep_selector: '[data-js-dependency]',
      css_dep_selector: '[data-css-dependency]',
      ie_fallback: false,
      version: '1.0',
      nocache: false
    };

    _classCallCheck(this, DynamicDepsLoader);

    // set defaults
    this.js_dir = args.jsdir;
    this.css_dir = args.cssdir;
    this.js_dep_selector = args.js_dep_selector;
    this.css_dep_selector = args.css_dep_selector;
    this.nocache = args.nocache;
    this.ie_fallback = args.ie_fallback;
    this.version = typeof args.version != 'undefined' ? args.version : '1.0';
    this.head = document.getElementsByTagName('head')[0]; // load dependecies

    if (this.ie_fallback) this.ie9Deps();else this.getDeps();
  }

  _createClass(DynamicDepsLoader, [{
    key: "scanJS",
    value: function scanJS() {
      // find the dependencies
      var elements = document.querySelectorAll(this.js_dep_selector); // bucket to fill of deps to load

      var dependencies = [];

      for (var i = 0; i < elements.length; i++) {
        // empties the item of the dependencies
        var item = [];
        var jsdeps = elements[i].dataset.jsDependency.split(','); // remove dependency attribute

        delete elements[i].dataset.jsDependency;

        for (var j = 0; j < jsdeps.length; j++) {
          item.push({
            url: this.js_dir + jsdeps[j].trim() + '?ver=' + this.version
          });
        }

        if (item.length) dependencies.push(item);
      }

      return dependencies;
    }
  }, {
    key: "scanCSS",
    value: function scanCSS() {
      var nocache = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var dep_els = document.querySelectorAll(this.css_dep_selector);
      var deps = [];
      var urls = [];

      for (var i = 0; i < dep_els.length; i++) {
        var item = {
          url: this.css_dir + dep_els[i].dataset.cssDependency + '?ver=' + this.version,
          execute: false,
          overwrite: nocache
        };
        var urltocheck = item.url;

        if (urls.indexOf(urltocheck) == -1) {
          // urltocheck not present
          deps.push(item);
          urls.push(urltocheck);
        }
      }

      return deps;
    }
  }, {
    key: "getDeps",
    value: function getDeps() {
      var _this = this;

      // for more details look at https://addyosmani.com/basket.js/#basketrequire        
      // scan components and find dependencies
      this.js_deps = this.scanJS();
      this.css_deps = this.scanCSS(this.nocache); // appends stylesheets

      if (this.css_deps.length) this.css_deps.forEach(function (dep) {
        return basket.require(dep).then(function (responses) {
          var css = responses[0].data;

          _this.appendStyleSheet(css);
        });
      }); // appends js with

      if (this.js_deps.length) for (var i = 0; i < this.js_deps.length; i++) {
        this.require(this.js_deps[i]);
      }
    }
  }, {
    key: "require",
    value: function require(deps) {
      var _this2 = this;

      var dep = deps.shift();
      if (deps.length) basket.require(dep).then(function () {
        return _this2.require(deps);
      }); // recursive this.require(dep[0])
      else basket.require(dep); // base            
    }
  }, {
    key: "appendStyleSheet",
    value: function appendStyleSheet(css) {
      var style = document.createElement('style');
      style.setAttribute('type', 'text/css');
      var head = document.getElementsByTagName('head')[0];
      head.appendChild(style);

      if (style.styleSheet) {
        style.styleSheet.cssText = css;
      } else if ('textContent' in style) {
        style.textContent = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }
    }
    /**
     * 
     * @param {string} type is the type of the element ('css' or 'js')
     * @param {string} url 
     */

  }, {
    key: "appendInHead",
    value: function appendInHead(type, url) {
      var element_name = {
        'js': 'script',
        'css': 'link'
      };
      var element = document.createElement(element_name[type]);

      switch (type) {
        case 'css':
          element.type = 'text/css';
          element.rel = 'stylesheet';
          element.href = url;
          break;

        case 'js':
          element.src = url;
          break;
      }

      this.head.appendChild(element);
    }
    /**
     * 
     * @param {*} type maybe 'css' or 'js'
     */

  }, {
    key: "ie9Scan",
    value: function ie9Scan(type) {
      var selector = {
        'js': this.js_dep_selector,
        'css': this.css_dep_selector
      };
      var attribute = 'data-' + type + '-dependency';
      var depdir = {
        'js': this.js_dir,
        'css': this.css_dir
      }; // find the dependencies

      var elements = document.querySelectorAll(selector[type]);
      console.log('elements', elements[0].getAttribute(attribute)); // bucket to fill of deps to load

      var dependencies = [];

      for (var i = 0; i < elements.length; i++) {
        // empties the item of the dependencies
        var item = [];
        var attribute_content = elements[i].getAttribute(attribute);
        var datadeps = attribute_content.indexOf(',') > -1 ? attribute_content.split(',') : attribute_content; // remove dependency attribute

        delete elements[i].removeAttribute(attribute); // populates the array of urls

        if (_typeof(datadeps) == 'object') for (var j = 0; j < datadeps.length; j++) {
          item.push({
            url: depdir[type] + datadeps[j].trim()
          });
        } else item = {
          url: depdir[type] + datadeps.trim()
        };
        dependencies.push(item);
      }

      return dependencies;
    }
  }, {
    key: "ie9Deps",
    value: function ie9Deps() {
      console.log('HI Internet Explorer');
      var js_deps = this.ie9Scan('js');
      var css_deps = this.ie9Scan('css'); // appends stylesheets

      for (var i = 0; i < css_deps.length; i++) {
        this.appendInHead('css', css_deps[i].url + '?ver=' + this.version);
      } // appends stylesheets


      for (i = 0; i < js_deps.length; i++) {
        for (var j = 0; j < js_deps[i].length; j++) {
          this.appendInHead('js', js_deps[i][j].url + '?ver=' + this.version);
        }
      }
    }
  }]);

  return DynamicDepsLoader;
}();
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/** 
 *  @class Scroller
 * 
 *  @description 
 *   Responsibilities:
 *   > give dimensions and positions of documents and viewport
 *   > give the scroll percentage of a viewport into elements
 *   > collect and execute actions related to the scroll event
 *   
 *  @author a.marzilli@f2innovation.com
 *  
 *  @copyright F2Innovation Srl
 *  
 *  @method getPosition(element)    - returns the position of the given element in the page
 *  @method getYinViewPort(element) - returns the position of the given element in the viewport
 *  @method getProgressIn(element)  - returns the percentage of element already covered by the viewport
 *  @method isInViewPort(element,offset) - true if the element is in viewport
 * 
 *  @property {array} actionStack[] - the array of actions to accomplish for each scroll event.
 *                                    Made to push in your custom actions. 
 *  @property {number} pageHeight - the actual height of the document
 *  @property {number} viewPortHeight - the actual height of the viewport
 *  @property {number} viewPortPosition - the current position of the viewport. The distance from the top of the page
 * 
 */
var Scroller = /*#__PURE__*/function () {
  function Scroller() {
    _classCallCheck(this, Scroller);

    // checkers for IE8 compatibility (consider them as private)
    this._supportPageOffset = window.pageYOffset !== undefined;
    this._isCSS1Compat = (document.compatMode || "") === "CSS1Compat"; // the actual height of the document

    this.pageHeight = 0; // the actual height of the viewport

    this.viewPortHeight = 0; // the current position of the viewport

    this.viewPortPosition = 0;
    /**
     *  This is the stack of callbacks activated on scroll
     *  we can add all the needed actions as callback functions
     */

    this.actionStack = [];
    var that = this;
    /**
     * adds the callback actions to the scroll event
     */

    this.actionStack.push(function () {
      that.setPageHeight.call(that);
      that.setViewPortHeight.call(that);
      that.setViewPortPosition.call(that);
    }); // initializes the object with the current values

    for (var i = 0; i < this.actionStack.length; i++) {
      this.actionStack[i]();
    }

    this._setScrollEventListener(); // initialize an observer


    this.observer = this._setIntersectionObserver(); // initialize an event

    this.onInViewPort = new CustomEvent('inviewport');
  } // setters for the properties


  _createClass(Scroller, [{
    key: "setPageHeight",
    value: function setPageHeight() {
      this.pageHeight = this._isCSS1Compat ? document.documentElement.scrollHeight : document.body.scrollHeight;
    }
  }, {
    key: "setViewPortHeight",
    value: function setViewPortHeight() {
      this.viewPortHeight = window.innerHeight;
    }
  }, {
    key: "setViewPortPosition",
    value: function setViewPortPosition() {
      this.viewPortPosition = parseInt(this._supportPageOffset ? window.pageYOffset : this._isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop, 10);
    }
  }, {
    key: "getPosition",

    /**
     * returns the position of an element
     */
    value: function getPosition(element) {
      return element.getBoundingClientRect().top + this.viewPortPosition;
    }
    /**
     * returns the distance between the top of the element   
     * and the top of the viewport
     */

  }, {
    key: "getYinViewPort",
    value: function getYinViewPort(element) {
      return element.getBoundingClientRect().top;
    }
    /** @method getProgressIn
     * 
     *  @returns the percentage of content zone already covered by the viewport.
     *  @default null if viewport is out of the zone
     */

  }, {
    key: "getProgressIn",
    value: function getProgressIn(element) {
      var element_position = this.getPosition(element);
      var zone = {
        height: element.scrollHeight,
        // top and bottom position of the zone
        top: element_position,
        bottom: element_position + element.scrollHeight,
        // positions where start and stop the percentage counter
        start: element_position - this.viewPortHeight,
        end: element_position + element.scrollHeight - this.viewPortHeight
        /*      
         
                    --- - --- - --- - --- - --- - --- - --- -  < -- zone.start  ---------    
                                                                                    ^                   
                                                                                    | viewport height
                                                                                    v
                       +-------------------------------+       < -- zone.top    ---------
                       |                               |
          Content -- > |                               |
                       |                               |
                    .-------------------------------------.
                    |                                     |   
         Viewport > |                                     | 
                    |--- - --- - --- - --- - --- - --- - -|- - < -- zone.end    ---------
                    '-------------------------------------'                         ^
                       |                               |                            | viewport height
                       |                               |                            v
                       +-------------------------------+       < -- zone.bottom ---------
             */

      }; // returns null if viewport is out of the zone

      if (this.viewPortPosition < zone.start || this.viewPortPosition > zone.bottom) return null; // returns 0 while viewport is not yet completelty in the zone

      if (this.viewPortPosition > zone.start && this.viewPortPosition < zone.top) return 0; // returns 100 while viewport is not yet completelty out of the zone

      if (this.viewPortPosition > zone.end && this.viewPortPosition < zone.bottom) return 100;
      var covered = this.viewPortPosition - zone.top;
      return parseInt(covered * 100 / (zone.height - this.viewPortHeight));
    }
    /**
     * returns true if the top-left corner of an element is currently in the viewport
     */

  }, {
    key: "isInViewPort",
    value: function isInViewPort(element) {
      var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      return element.getBoundingClientRect().top < this.viewPortHeight + offset;
    }
    /*
     * Initialize a unique observer (if possible) and create
     * a workaround to keep track of the callbacks for each
     * entry of the observer
     */

  }, {
    key: "_setIntersectionObserver",
    value: function _setIntersectionObserver() {
      if (typeof IntersectionObserver == 'undefined' || !'IntersectionObserver' in window || !'IntersectionObserverEntry' in window || !'intersectionRatio' in window.IntersectionObserverEntry.prototype) return null;else {
        var options = {
          root: document.body,
          //rootMargin: '2000px',
          // root:  document.querySelector('#scrollArea'),
          // rootMargin: '0px',
          threshold: 1.0
        }; // this is a trick to keep track of callbacks when
        // using IntersectionObserver

        if (!window.observerTGTCallbacks) window.observerTGTCallbacks = [];
        this.observerCallbacksID = window.observerTGTCallbacks.length;
        return new IntersectionObserver(this._observerCallback);
      }
    }
    /**
     * Handles the event fired when a target element goes 
     * into the viewport
     * 
     * @param {*} target 
     * @param {*} callback     
     */

  }, {
    key: "onElementInViewport",
    value: function onElementInViewport(target, callback) {
      var once = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

      if (this.observer == null) {
        // use a sort of polyfill
        target.addEventListener('inviewport', callback);

        this._polyfill(target, callback, once);
      } else {
        target.setAttribute('data-scroller-observer-callback-id', this.observerCallbacksID);
        window.observerTGTCallbacks[this.observerCallbacksID++] = callback;
        this.observer.observe(target);
      }
    }
  }, {
    key: "_polyfill",
    value: function _polyfill(target, callback, once) {
      var _this = this;

      this.actionStack.push(function () {
        if (_this.isInViewPort(target)) {
          target.dispatchEvent(_this.onInViewPort);
          if (once) target.removeEventListener('inviewport', callback);
        }
      });
    }
  }, {
    key: "_observerCallback",
    value: function _observerCallback(entries, observer) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          window.observerTGTCallbacks[entry.target.dataset.scrollerObserverCallbackId]();
          observer.unobserve(entry.target);
        }
      });
    }
  }, {
    key: "_setScrollEventListener",
    value: function _setScrollEventListener() {
      var self = this;
      window.addEventListener('scroll', function (e) {
        for (var i = 0; i < self.actionStack.length; i++) {
          self.actionStack[i]();
        }
      }); // if ie8

      /*
      window.onscroll = function() {
          for(var i=0;i<this.actionStack.length;i++) this.actionStack[i](); 
      }
      */
    }
  }]);

  return Scroller;
}();

(function () {
  if (typeof window.CustomEvent === "function") return false;

  function CustomEvent(event, params) {
    params = params || {
      bubbles: false,
      cancelable: false,
      detail: null
    };
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }

  window.CustomEvent = CustomEvent;
})();
/*!
 * @overview RSVP - a tiny implementation of Promises/A+.
 * @copyright Copyright (c) 2016 Yehuda Katz, Tom Dale, Stefan Penner and contributors
 * @license   Licensed under MIT license
 *            See https://raw.githubusercontent.com/tildeio/rsvp.js/master/LICENSE
 * @version   4.8.4+ff10049b
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.RSVP = {})));
}(this, (function (exports) { 'use strict';

  function callbacksFor(object) {
    var callbacks = object._promiseCallbacks;

    if (!callbacks) {
      callbacks = object._promiseCallbacks = {};
    }

    return callbacks;
  }

  /**
    @class EventTarget
    @for rsvp
    @public
  */
  var EventTarget = {

    /**
      `EventTarget.mixin` extends an object with EventTarget methods. For
      Example:
       ```javascript
      import EventTarget from 'rsvp';
       let object = {};
       EventTarget.mixin(object);
       object.on('finished', function(event) {
        // handle event
      });
       object.trigger('finished', { detail: value });
      ```
       `EventTarget.mixin` also works with prototypes:
       ```javascript
      import EventTarget from 'rsvp';
       let Person = function() {};
      EventTarget.mixin(Person.prototype);
       let yehuda = new Person();
      let tom = new Person();
       yehuda.on('poke', function(event) {
        console.log('Yehuda says OW');
      });
       tom.on('poke', function(event) {
        console.log('Tom says OW');
      });
       yehuda.trigger('poke');
      tom.trigger('poke');
      ```
       @method mixin
      @for rsvp
      @private
      @param {Object} object object to extend with EventTarget methods
    */
    mixin: function (object) {
      object.on = this.on;
      object.off = this.off;
      object.trigger = this.trigger;
      object._promiseCallbacks = undefined;
      return object;
    },


    /**
      Registers a callback to be executed when `eventName` is triggered
       ```javascript
      object.on('event', function(eventInfo){
        // handle the event
      });
       object.trigger('event');
      ```
       @method on
      @for EventTarget
      @private
      @param {String} eventName name of the event to listen for
      @param {Function} callback function to be called when the event is triggered.
    */
    on: function (eventName, callback) {
      if (typeof callback !== 'function') {
        throw new TypeError('Callback must be a function');
      }

      var allCallbacks = callbacksFor(this);
      var callbacks = allCallbacks[eventName];

      if (!callbacks) {
        callbacks = allCallbacks[eventName] = [];
      }

      if (callbacks.indexOf(callback) === -1) {
        callbacks.push(callback);
      }
    },


    /**
      You can use `off` to stop firing a particular callback for an event:
       ```javascript
      function doStuff() { // do stuff! }
      object.on('stuff', doStuff);
       object.trigger('stuff'); // doStuff will be called
       // Unregister ONLY the doStuff callback
      object.off('stuff', doStuff);
      object.trigger('stuff'); // doStuff will NOT be called
      ```
       If you don't pass a `callback` argument to `off`, ALL callbacks for the
      event will not be executed when the event fires. For example:
       ```javascript
      let callback1 = function(){};
      let callback2 = function(){};
       object.on('stuff', callback1);
      object.on('stuff', callback2);
       object.trigger('stuff'); // callback1 and callback2 will be executed.
       object.off('stuff');
      object.trigger('stuff'); // callback1 and callback2 will not be executed!
      ```
       @method off
      @for rsvp
      @private
      @param {String} eventName event to stop listening to
      @param {Function} [callback] optional argument. If given, only the function
      given will be removed from the event's callback queue. If no `callback`
      argument is given, all callbacks will be removed from the event's callback
      queue.
    */
    off: function (eventName, callback) {
      var allCallbacks = callbacksFor(this);

      if (!callback) {
        allCallbacks[eventName] = [];
        return;
      }

      var callbacks = allCallbacks[eventName];
      var index = callbacks.indexOf(callback);

      if (index !== -1) {
        callbacks.splice(index, 1);
      }
    },


    /**
      Use `trigger` to fire custom events. For example:
       ```javascript
      object.on('foo', function(){
        console.log('foo event happened!');
      });
      object.trigger('foo');
      // 'foo event happened!' logged to the console
      ```
       You can also pass a value as a second argument to `trigger` that will be
      passed as an argument to all event listeners for the event:
       ```javascript
      object.on('foo', function(value){
        console.log(value.name);
      });
       object.trigger('foo', { name: 'bar' });
      // 'bar' logged to the console
      ```
       @method trigger
      @for rsvp
      @private
      @param {String} eventName name of the event to be triggered
      @param {*} [options] optional value to be passed to any event handlers for
      the given `eventName`
    */
    trigger: function (eventName, options, label) {
      var allCallbacks = callbacksFor(this);

      var callbacks = allCallbacks[eventName];
      if (callbacks) {
        // Don't cache the callbacks.length since it may grow
        var callback = void 0;
        for (var i = 0; i < callbacks.length; i++) {
          callback = callbacks[i];
          callback(options, label);
        }
      }
    }
  };

  var config = {
    instrument: false
  };

  EventTarget['mixin'](config);

  function configure(name, value) {
    if (arguments.length === 2) {
      config[name] = value;
    } else {
      return config[name];
    }
  }

  var queue = [];

  function scheduleFlush() {
    setTimeout(function () {
      for (var i = 0; i < queue.length; i++) {
        var entry = queue[i];

        var payload = entry.payload;

        payload.guid = payload.key + payload.id;
        payload.childGuid = payload.key + payload.childId;
        if (payload.error) {
          payload.stack = payload.error.stack;
        }

        config['trigger'](entry.name, entry.payload);
      }
      queue.length = 0;
    }, 50);
  }

  function instrument(eventName, promise, child) {
    if (1 === queue.push({
      name: eventName,
      payload: {
        key: promise._guidKey,
        id: promise._id,
        eventName: eventName,
        detail: promise._result,
        childId: child && child._id,
        label: promise._label,
        timeStamp: Date.now(),
        error: config["instrument-with-stack"] ? new Error(promise._label) : null
      } })) {
      scheduleFlush();
    }
  }

  /**
    `Promise.resolve` returns a promise that will become resolved with the
    passed `value`. It is shorthand for the following:

    ```javascript
    import Promise from 'rsvp';

    let promise = new Promise(function(resolve, reject){
      resolve(1);
    });

    promise.then(function(value){
      // value === 1
    });
    ```

    Instead of writing the above, your code now simply becomes the following:

    ```javascript
    import Promise from 'rsvp';

    let promise = RSVP.Promise.resolve(1);

    promise.then(function(value){
      // value === 1
    });
    ```

    @method resolve
    @for Promise
    @static
    @param {*} object value that the returned promise will be resolved with
    @param {String} [label] optional string for identifying the returned promise.
    Useful for tooling.
    @return {Promise} a promise that will become fulfilled with the given
    `value`
  */
  function resolve$$1(object, label) {
    /*jshint validthis:true */
    var Constructor = this;

    if (object && typeof object === 'object' && object.constructor === Constructor) {
      return object;
    }

    var promise = new Constructor(noop, label);
    resolve$1(promise, object);
    return promise;
  }

  function withOwnPromise() {
    return new TypeError('A promises callback cannot return that same promise.');
  }

  function objectOrFunction(x) {
    var type = typeof x;
    return x !== null && (type === 'object' || type === 'function');
  }

  function noop() {}

  var PENDING = void 0;
  var FULFILLED = 1;
  var REJECTED = 2;

  var TRY_CATCH_ERROR = { error: null };

  function getThen(promise) {
    try {
      return promise.then;
    } catch (error) {
      TRY_CATCH_ERROR.error = error;
      return TRY_CATCH_ERROR;
    }
  }

  var tryCatchCallback = void 0;
  function tryCatcher() {
    try {
      var target = tryCatchCallback;
      tryCatchCallback = null;
      return target.apply(this, arguments);
    } catch (e) {
      TRY_CATCH_ERROR.error = e;
      return TRY_CATCH_ERROR;
    }
  }

  function tryCatch(fn) {
    tryCatchCallback = fn;
    return tryCatcher;
  }

  function handleForeignThenable(promise, thenable, then$$1) {
    config.async(function (promise) {
      var sealed = false;
      var result = tryCatch(then$$1).call(thenable, function (value) {
        if (sealed) {
          return;
        }
        sealed = true;
        if (thenable === value) {
          fulfill(promise, value);
        } else {
          resolve$1(promise, value);
        }
      }, function (reason) {
        if (sealed) {
          return;
        }
        sealed = true;

        reject(promise, reason);
      }, 'Settle: ' + (promise._label || ' unknown promise'));

      if (!sealed && result === TRY_CATCH_ERROR) {
        sealed = true;
        var error = TRY_CATCH_ERROR.error;
        TRY_CATCH_ERROR.error = null;
        reject(promise, error);
      }
    }, promise);
  }

  function handleOwnThenable(promise, thenable) {
    if (thenable._state === FULFILLED) {
      fulfill(promise, thenable._result);
    } else if (thenable._state === REJECTED) {
      thenable._onError = null;
      reject(promise, thenable._result);
    } else {
      subscribe(thenable, undefined, function (value) {
        if (thenable === value) {
          fulfill(promise, value);
        } else {
          resolve$1(promise, value);
        }
      }, function (reason) {
        return reject(promise, reason);
      });
    }
  }

  function handleMaybeThenable(promise, maybeThenable, then$$1) {
    var isOwnThenable = maybeThenable.constructor === promise.constructor && then$$1 === then && promise.constructor.resolve === resolve$$1;

    if (isOwnThenable) {
      handleOwnThenable(promise, maybeThenable);
    } else if (then$$1 === TRY_CATCH_ERROR) {
      var error = TRY_CATCH_ERROR.error;
      TRY_CATCH_ERROR.error = null;
      reject(promise, error);
    } else if (typeof then$$1 === 'function') {
      handleForeignThenable(promise, maybeThenable, then$$1);
    } else {
      fulfill(promise, maybeThenable);
    }
  }

  function resolve$1(promise, value) {
    if (promise === value) {
      fulfill(promise, value);
    } else if (objectOrFunction(value)) {
      handleMaybeThenable(promise, value, getThen(value));
    } else {
      fulfill(promise, value);
    }
  }

  function publishRejection(promise) {
    if (promise._onError) {
      promise._onError(promise._result);
    }

    publish(promise);
  }

  function fulfill(promise, value) {
    if (promise._state !== PENDING) {
      return;
    }

    promise._result = value;
    promise._state = FULFILLED;

    if (promise._subscribers.length === 0) {
      if (config.instrument) {
        instrument('fulfilled', promise);
      }
    } else {
      config.async(publish, promise);
    }
  }

  function reject(promise, reason) {
    if (promise._state !== PENDING) {
      return;
    }
    promise._state = REJECTED;
    promise._result = reason;
    config.async(publishRejection, promise);
  }

  function subscribe(parent, child, onFulfillment, onRejection) {
    var subscribers = parent._subscribers;
    var length = subscribers.length;

    parent._onError = null;

    subscribers[length] = child;
    subscribers[length + FULFILLED] = onFulfillment;
    subscribers[length + REJECTED] = onRejection;

    if (length === 0 && parent._state) {
      config.async(publish, parent);
    }
  }

  function publish(promise) {
    var subscribers = promise._subscribers;
    var settled = promise._state;

    if (config.instrument) {
      instrument(settled === FULFILLED ? 'fulfilled' : 'rejected', promise);
    }

    if (subscribers.length === 0) {
      return;
    }

    var child = void 0,
        callback = void 0,
        result = promise._result;

    for (var i = 0; i < subscribers.length; i += 3) {
      child = subscribers[i];
      callback = subscribers[i + settled];

      if (child) {
        invokeCallback(settled, child, callback, result);
      } else {
        callback(result);
      }
    }

    promise._subscribers.length = 0;
  }

  function invokeCallback(state, promise, callback, result) {
    var hasCallback = typeof callback === 'function';
    var value = void 0;

    if (hasCallback) {
      value = tryCatch(callback)(result);
    } else {
      value = result;
    }

    if (promise._state !== PENDING) {
      // noop
    } else if (value === promise) {
      reject(promise, withOwnPromise());
    } else if (value === TRY_CATCH_ERROR) {
      var error = TRY_CATCH_ERROR.error;
      TRY_CATCH_ERROR.error = null; // release
      reject(promise, error);
    } else if (hasCallback) {
      resolve$1(promise, value);
    } else if (state === FULFILLED) {
      fulfill(promise, value);
    } else if (state === REJECTED) {
      reject(promise, value);
    }
  }

  function initializePromise(promise, resolver) {
    var resolved = false;
    try {
      resolver(function (value) {
        if (resolved) {
          return;
        }
        resolved = true;
        resolve$1(promise, value);
      }, function (reason) {
        if (resolved) {
          return;
        }
        resolved = true;
        reject(promise, reason);
      });
    } catch (e) {
      reject(promise, e);
    }
  }

  function then(onFulfillment, onRejection, label) {
    var parent = this;
    var state = parent._state;

    if (state === FULFILLED && !onFulfillment || state === REJECTED && !onRejection) {
      config.instrument && instrument('chained', parent, parent);
      return parent;
    }

    parent._onError = null;

    var child = new parent.constructor(noop, label);
    var result = parent._result;

    config.instrument && instrument('chained', parent, child);

    if (state === PENDING) {
      subscribe(parent, child, onFulfillment, onRejection);
    } else {
      var callback = state === FULFILLED ? onFulfillment : onRejection;
      config.async(function () {
        return invokeCallback(state, child, callback, result);
      });
    }

    return child;
  }

  var Enumerator = function () {
    function Enumerator(Constructor, input, abortOnReject, label) {
      this._instanceConstructor = Constructor;
      this.promise = new Constructor(noop, label);
      this._abortOnReject = abortOnReject;
      this._isUsingOwnPromise = Constructor === Promise;
      this._isUsingOwnResolve = Constructor.resolve === resolve$$1;

      this._init.apply(this, arguments);
    }

    Enumerator.prototype._init = function _init(Constructor, input) {
      var len = input.length || 0;
      this.length = len;
      this._remaining = len;
      this._result = new Array(len);

      this._enumerate(input);
    };

    Enumerator.prototype._enumerate = function _enumerate(input) {
      var length = this.length;
      var promise = this.promise;

      for (var i = 0; promise._state === PENDING && i < length; i++) {
        this._eachEntry(input[i], i, true);
      }
      this._checkFullfillment();
    };

    Enumerator.prototype._checkFullfillment = function _checkFullfillment() {
      if (this._remaining === 0) {
        var result = this._result;
        fulfill(this.promise, result);
        this._result = null;
      }
    };

    Enumerator.prototype._settleMaybeThenable = function _settleMaybeThenable(entry, i, firstPass) {
      var c = this._instanceConstructor;

      if (this._isUsingOwnResolve) {
        var then$$1 = getThen(entry);

        if (then$$1 === then && entry._state !== PENDING) {
          entry._onError = null;
          this._settledAt(entry._state, i, entry._result, firstPass);
        } else if (typeof then$$1 !== 'function') {
          this._settledAt(FULFILLED, i, entry, firstPass);
        } else if (this._isUsingOwnPromise) {
          var promise = new c(noop);
          handleMaybeThenable(promise, entry, then$$1);
          this._willSettleAt(promise, i, firstPass);
        } else {
          this._willSettleAt(new c(function (resolve) {
            return resolve(entry);
          }), i, firstPass);
        }
      } else {
        this._willSettleAt(c.resolve(entry), i, firstPass);
      }
    };

    Enumerator.prototype._eachEntry = function _eachEntry(entry, i, firstPass) {
      if (entry !== null && typeof entry === 'object') {
        this._settleMaybeThenable(entry, i, firstPass);
      } else {
        this._setResultAt(FULFILLED, i, entry, firstPass);
      }
    };

    Enumerator.prototype._settledAt = function _settledAt(state, i, value, firstPass) {
      var promise = this.promise;

      if (promise._state === PENDING) {
        if (this._abortOnReject && state === REJECTED) {
          reject(promise, value);
        } else {
          this._setResultAt(state, i, value, firstPass);
          this._checkFullfillment();
        }
      }
    };

    Enumerator.prototype._setResultAt = function _setResultAt(state, i, value, firstPass) {
      this._remaining--;
      this._result[i] = value;
    };

    Enumerator.prototype._willSettleAt = function _willSettleAt(promise, i, firstPass) {
      var _this = this;

      subscribe(promise, undefined, function (value) {
        return _this._settledAt(FULFILLED, i, value, firstPass);
      }, function (reason) {
        return _this._settledAt(REJECTED, i, reason, firstPass);
      });
    };

    return Enumerator;
  }();


  function setSettledResult(state, i, value) {
    this._remaining--;
    if (state === FULFILLED) {
      this._result[i] = {
        state: 'fulfilled',
        value: value
      };
    } else {
      this._result[i] = {
        state: 'rejected',
        reason: value
      };
    }
  }

  /**
    `Promise.all` accepts an array of promises, and returns a new promise which
    is fulfilled with an array of fulfillment values for the passed promises, or
    rejected with the reason of the first passed promise to be rejected. It casts all
    elements of the passed iterable to promises as it runs this algorithm.

    Example:

    ```javascript
    import Promise, { resolve } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = resolve(2);
    let promise3 = resolve(3);
    let promises = [ promise1, promise2, promise3 ];

    Promise.all(promises).then(function(array){
      // The array here would be [ 1, 2, 3 ];
    });
    ```

    If any of the `promises` given to `RSVP.all` are rejected, the first promise
    that is rejected will be given as an argument to the returned promises's
    rejection handler. For example:

    Example:

    ```javascript
    import Promise, { resolve, reject } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = reject(new Error("2"));
    let promise3 = reject(new Error("3"));
    let promises = [ promise1, promise2, promise3 ];

    Promise.all(promises).then(function(array){
      // Code here never runs because there are rejected promises!
    }, function(error) {
      // error.message === "2"
    });
    ```

    @method all
    @for Promise
    @param {Array} entries array of promises
    @param {String} [label] optional string for labeling the promise.
    Useful for tooling.
    @return {Promise} promise that is fulfilled when all `promises` have been
    fulfilled, or rejected if any of them become rejected.
    @static
  */
  function all(entries, label) {
    if (!Array.isArray(entries)) {
      return this.reject(new TypeError("Promise.all must be called with an array"), label);
    }
    return new Enumerator(this, entries, true /* abort on reject */, label).promise;
  }

  /**
    `Promise.race` returns a new promise which is settled in the same way as the
    first passed promise to settle.

    Example:

    ```javascript
    import Promise from 'rsvp';

    let promise1 = new Promise(function(resolve, reject){
      setTimeout(function(){
        resolve('promise 1');
      }, 200);
    });

    let promise2 = new Promise(function(resolve, reject){
      setTimeout(function(){
        resolve('promise 2');
      }, 100);
    });

    Promise.race([promise1, promise2]).then(function(result){
      // result === 'promise 2' because it was resolved before promise1
      // was resolved.
    });
    ```

    `Promise.race` is deterministic in that only the state of the first
    settled promise matters. For example, even if other promises given to the
    `promises` array argument are resolved, but the first settled promise has
    become rejected before the other promises became fulfilled, the returned
    promise will become rejected:

    ```javascript
    import Promise from 'rsvp';

    let promise1 = new Promise(function(resolve, reject){
      setTimeout(function(){
        resolve('promise 1');
      }, 200);
    });

    let promise2 = new Promise(function(resolve, reject){
      setTimeout(function(){
        reject(new Error('promise 2'));
      }, 100);
    });

    Promise.race([promise1, promise2]).then(function(result){
      // Code here never runs
    }, function(reason){
      // reason.message === 'promise 2' because promise 2 became rejected before
      // promise 1 became fulfilled
    });
    ```

    An example real-world use case is implementing timeouts:

    ```javascript
    import Promise from 'rsvp';

    Promise.race([ajax('foo.json'), timeout(5000)])
    ```

    @method race
    @for Promise
    @static
    @param {Array} entries array of promises to observe
    @param {String} [label] optional string for describing the promise returned.
    Useful for tooling.
    @return {Promise} a promise which settles in the same way as the first passed
    promise to settle.
  */
  function race(entries, label) {
    /*jshint validthis:true */
    var Constructor = this;

    var promise = new Constructor(noop, label);

    if (!Array.isArray(entries)) {
      reject(promise, new TypeError('Promise.race must be called with an array'));
      return promise;
    }

    for (var i = 0; promise._state === PENDING && i < entries.length; i++) {
      subscribe(Constructor.resolve(entries[i]), undefined, function (value) {
        return resolve$1(promise, value);
      }, function (reason) {
        return reject(promise, reason);
      });
    }

    return promise;
  }

  /**
    `Promise.reject` returns a promise rejected with the passed `reason`.
    It is shorthand for the following:

    ```javascript
    import Promise from 'rsvp';

    let promise = new Promise(function(resolve, reject){
      reject(new Error('WHOOPS'));
    });

    promise.then(function(value){
      // Code here doesn't run because the promise is rejected!
    }, function(reason){
      // reason.message === 'WHOOPS'
    });
    ```

    Instead of writing the above, your code now simply becomes the following:

    ```javascript
    import Promise from 'rsvp';

    let promise = Promise.reject(new Error('WHOOPS'));

    promise.then(function(value){
      // Code here doesn't run because the promise is rejected!
    }, function(reason){
      // reason.message === 'WHOOPS'
    });
    ```

    @method reject
    @for Promise
    @static
    @param {*} reason value that the returned promise will be rejected with.
    @param {String} [label] optional string for identifying the returned promise.
    Useful for tooling.
    @return {Promise} a promise rejected with the given `reason`.
  */
  function reject$1(reason, label) {
    /*jshint validthis:true */
    var Constructor = this;
    var promise = new Constructor(noop, label);
    reject(promise, reason);
    return promise;
  }

  var guidKey = 'rsvp_' + Date.now() + '-';
  var counter = 0;

  function needsResolver() {
    throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
  }

  function needsNew() {
    throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
  }

  /**
    Promise objects represent the eventual result of an asynchronous operation. The
    primary way of interacting with a promise is through its `then` method, which
    registers callbacks to receive either a promise’s eventual value or the reason
    why the promise cannot be fulfilled.

    Terminology
    -----------

    - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
    - `thenable` is an object or function that defines a `then` method.
    - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
    - `exception` is a value that is thrown using the throw statement.
    - `reason` is a value that indicates why a promise was rejected.
    - `settled` the final resting state of a promise, fulfilled or rejected.

    A promise can be in one of three states: pending, fulfilled, or rejected.

    Promises that are fulfilled have a fulfillment value and are in the fulfilled
    state.  Promises that are rejected have a rejection reason and are in the
    rejected state.  A fulfillment value is never a thenable.

    Promises can also be said to *resolve* a value.  If this value is also a
    promise, then the original promise's settled state will match the value's
    settled state.  So a promise that *resolves* a promise that rejects will
    itself reject, and a promise that *resolves* a promise that fulfills will
    itself fulfill.


    Basic Usage:
    ------------

    ```js
    let promise = new Promise(function(resolve, reject) {
      // on success
      resolve(value);

      // on failure
      reject(reason);
    });

    promise.then(function(value) {
      // on fulfillment
    }, function(reason) {
      // on rejection
    });
    ```

    Advanced Usage:
    ---------------

    Promises shine when abstracting away asynchronous interactions such as
    `XMLHttpRequest`s.

    ```js
    function getJSON(url) {
      return new Promise(function(resolve, reject){
        let xhr = new XMLHttpRequest();

        xhr.open('GET', url);
        xhr.onreadystatechange = handler;
        xhr.responseType = 'json';
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.send();

        function handler() {
          if (this.readyState === this.DONE) {
            if (this.status === 200) {
              resolve(this.response);
            } else {
              reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
            }
          }
        };
      });
    }

    getJSON('/posts.json').then(function(json) {
      // on fulfillment
    }, function(reason) {
      // on rejection
    });
    ```

    Unlike callbacks, promises are great composable primitives.

    ```js
    Promise.all([
      getJSON('/posts'),
      getJSON('/comments')
    ]).then(function(values){
      values[0] // => postsJSON
      values[1] // => commentsJSON

      return values;
    });
    ```

    @class Promise
    @public
    @param {function} resolver
    @param {String} [label] optional string for labeling the promise.
    Useful for tooling.
    @constructor
  */

  var Promise = function () {
    function Promise(resolver, label) {
      this._id = counter++;
      this._label = label;
      this._state = undefined;
      this._result = undefined;
      this._subscribers = [];

      config.instrument && instrument('created', this);

      if (noop !== resolver) {
        typeof resolver !== 'function' && needsResolver();
        this instanceof Promise ? initializePromise(this, resolver) : needsNew();
      }
    }

    Promise.prototype._onError = function _onError(reason) {
      var _this = this;

      config.after(function () {
        if (_this._onError) {
          config.trigger('error', reason, _this._label);
        }
      });
    };

    /**
      `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
      as the catch block of a try/catch statement.
    
      ```js
      function findAuthor(){
        throw new Error('couldn\'t find that author');
      }
    
      // synchronous
      try {
        findAuthor();
      } catch(reason) {
        // something went wrong
      }
    
      // async with promises
      findAuthor().catch(function(reason){
        // something went wrong
      });
      ```
    
      @method catch
      @param {Function} onRejection
      @param {String} [label] optional string for labeling the promise.
      Useful for tooling.
      @return {Promise}
    */


    Promise.prototype.catch = function _catch(onRejection, label) {
      return this.then(undefined, onRejection, label);
    };

    /**
      `finally` will be invoked regardless of the promise's fate just as native
      try/catch/finally behaves
    
      Synchronous example:
    
      ```js
      findAuthor() {
        if (Math.random() > 0.5) {
          throw new Error();
        }
        return new Author();
      }
    
      try {
        return findAuthor(); // succeed or fail
      } catch(error) {
        return findOtherAuthor();
      } finally {
        // always runs
        // doesn't affect the return value
      }
      ```
    
      Asynchronous example:
    
      ```js
      findAuthor().catch(function(reason){
        return findOtherAuthor();
      }).finally(function(){
        // author was either found, or not
      });
      ```
    
      @method finally
      @param {Function} callback
      @param {String} [label] optional string for labeling the promise.
      Useful for tooling.
      @return {Promise}
    */


    Promise.prototype.finally = function _finally(callback, label) {
      var promise = this;
      var constructor = promise.constructor;

      if (typeof callback === 'function') {
        return promise.then(function (value) {
          return constructor.resolve(callback()).then(function () {
            return value;
          });
        }, function (reason) {
          return constructor.resolve(callback()).then(function () {
            throw reason;
          });
        });
      }

      return promise.then(callback, callback);
    };

    return Promise;
  }();

  Promise.cast = resolve$$1; // deprecated
  Promise.all = all;
  Promise.race = race;
  Promise.resolve = resolve$$1;
  Promise.reject = reject$1;

  Promise.prototype._guidKey = guidKey;

  /**
    The primary way of interacting with a promise is through its `then` method,
    which registers callbacks to receive either a promise's eventual value or the
    reason why the promise cannot be fulfilled.

    ```js
    findUser().then(function(user){
      // user is available
    }, function(reason){
      // user is unavailable, and you are given the reason why
    });
    ```

    Chaining
    --------

    The return value of `then` is itself a promise.  This second, 'downstream'
    promise is resolved with the return value of the first promise's fulfillment
    or rejection handler, or rejected if the handler throws an exception.

    ```js
    findUser().then(function (user) {
      return user.name;
    }, function (reason) {
      return 'default name';
    }).then(function (userName) {
      // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
      // will be `'default name'`
    });

    findUser().then(function (user) {
      throw new Error('Found user, but still unhappy');
    }, function (reason) {
      throw new Error('`findUser` rejected and we\'re unhappy');
    }).then(function (value) {
      // never reached
    }, function (reason) {
      // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
      // If `findUser` rejected, `reason` will be '`findUser` rejected and we\'re unhappy'.
    });
    ```
    If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.

    ```js
    findUser().then(function (user) {
      throw new PedagogicalException('Upstream error');
    }).then(function (value) {
      // never reached
    }).then(function (value) {
      // never reached
    }, function (reason) {
      // The `PedgagocialException` is propagated all the way down to here
    });
    ```

    Assimilation
    ------------

    Sometimes the value you want to propagate to a downstream promise can only be
    retrieved asynchronously. This can be achieved by returning a promise in the
    fulfillment or rejection handler. The downstream promise will then be pending
    until the returned promise is settled. This is called *assimilation*.

    ```js
    findUser().then(function (user) {
      return findCommentsByAuthor(user);
    }).then(function (comments) {
      // The user's comments are now available
    });
    ```

    If the assimliated promise rejects, then the downstream promise will also reject.

    ```js
    findUser().then(function (user) {
      return findCommentsByAuthor(user);
    }).then(function (comments) {
      // If `findCommentsByAuthor` fulfills, we'll have the value here
    }, function (reason) {
      // If `findCommentsByAuthor` rejects, we'll have the reason here
    });
    ```

    Simple Example
    --------------

    Synchronous Example

    ```javascript
    let result;

    try {
      result = findResult();
      // success
    } catch(reason) {
      // failure
    }
    ```

    Errback Example

    ```js
    findResult(function(result, err){
      if (err) {
        // failure
      } else {
        // success
      }
    });
    ```

    Promise Example;

    ```javascript
    findResult().then(function(result){
      // success
    }, function(reason){
      // failure
    });
    ```

    Advanced Example
    --------------

    Synchronous Example

    ```javascript
    let author, books;

    try {
      author = findAuthor();
      books  = findBooksByAuthor(author);
      // success
    } catch(reason) {
      // failure
    }
    ```

    Errback Example

    ```js

    function foundBooks(books) {

    }

    function failure(reason) {

    }

    findAuthor(function(author, err){
      if (err) {
        failure(err);
        // failure
      } else {
        try {
          findBoooksByAuthor(author, function(books, err) {
            if (err) {
              failure(err);
            } else {
              try {
                foundBooks(books);
              } catch(reason) {
                failure(reason);
              }
            }
          });
        } catch(error) {
          failure(err);
        }
        // success
      }
    });
    ```

    Promise Example;

    ```javascript
    findAuthor().
      then(findBooksByAuthor).
      then(function(books){
        // found books
    }).catch(function(reason){
      // something went wrong
    });
    ```

    @method then
    @param {Function} onFulfillment
    @param {Function} onRejection
    @param {String} [label] optional string for labeling the promise.
    Useful for tooling.
    @return {Promise}
  */
  Promise.prototype.then = then;

  function makeObject(_, argumentNames) {
    var obj = {};
    var length = _.length;
    var args = new Array(length);

    for (var x = 0; x < length; x++) {
      args[x] = _[x];
    }

    for (var i = 0; i < argumentNames.length; i++) {
      var name = argumentNames[i];
      obj[name] = args[i + 1];
    }

    return obj;
  }

  function arrayResult(_) {
    var length = _.length;
    var args = new Array(length - 1);

    for (var i = 1; i < length; i++) {
      args[i - 1] = _[i];
    }

    return args;
  }

  function wrapThenable(then, promise) {
    return {
      then: function (onFulFillment, onRejection) {
        return then.call(promise, onFulFillment, onRejection);
      }
    };
  }

  /**
    `denodeify` takes a 'node-style' function and returns a function that
    will return an `Promise`. You can use `denodeify` in Node.js or the
    browser when you'd prefer to use promises over using callbacks. For example,
    `denodeify` transforms the following:

    ```javascript
    let fs = require('fs');

    fs.readFile('myfile.txt', function(err, data){
      if (err) return handleError(err);
      handleData(data);
    });
    ```

    into:

    ```javascript
    let fs = require('fs');
    let readFile = denodeify(fs.readFile);

    readFile('myfile.txt').then(handleData, handleError);
    ```

    If the node function has multiple success parameters, then `denodeify`
    just returns the first one:

    ```javascript
    let request = denodeify(require('request'));

    request('http://example.com').then(function(res) {
      // ...
    });
    ```

    However, if you need all success parameters, setting `denodeify`'s
    second parameter to `true` causes it to return all success parameters
    as an array:

    ```javascript
    let request = denodeify(require('request'), true);

    request('http://example.com').then(function(result) {
      // result[0] -> res
      // result[1] -> body
    });
    ```

    Or if you pass it an array with names it returns the parameters as a hash:

    ```javascript
    let request = denodeify(require('request'), ['res', 'body']);

    request('http://example.com').then(function(result) {
      // result.res
      // result.body
    });
    ```

    Sometimes you need to retain the `this`:

    ```javascript
    let app = require('express')();
    let render = denodeify(app.render.bind(app));
    ```

    The denodified function inherits from the original function. It works in all
    environments, except IE 10 and below. Consequently all properties of the original
    function are available to you. However, any properties you change on the
    denodeified function won't be changed on the original function. Example:

    ```javascript
    let request = denodeify(require('request')),
        cookieJar = request.jar(); // <- Inheritance is used here

    request('http://example.com', {jar: cookieJar}).then(function(res) {
      // cookieJar.cookies holds now the cookies returned by example.com
    });
    ```

    Using `denodeify` makes it easier to compose asynchronous operations instead
    of using callbacks. For example, instead of:

    ```javascript
    let fs = require('fs');

    fs.readFile('myfile.txt', function(err, data){
      if (err) { ... } // Handle error
      fs.writeFile('myfile2.txt', data, function(err){
        if (err) { ... } // Handle error
        console.log('done')
      });
    });
    ```

    you can chain the operations together using `then` from the returned promise:

    ```javascript
    let fs = require('fs');
    let readFile = denodeify(fs.readFile);
    let writeFile = denodeify(fs.writeFile);

    readFile('myfile.txt').then(function(data){
      return writeFile('myfile2.txt', data);
    }).then(function(){
      console.log('done')
    }).catch(function(error){
      // Handle error
    });
    ```

    @method denodeify
    @public
    @static
    @for rsvp
    @param {Function} nodeFunc a 'node-style' function that takes a callback as
    its last argument. The callback expects an error to be passed as its first
    argument (if an error occurred, otherwise null), and the value from the
    operation as its second argument ('function(err, value){ }').
    @param {Boolean|Array} [options] An optional paramter that if set
    to `true` causes the promise to fulfill with the callback's success arguments
    as an array. This is useful if the node function has multiple success
    paramters. If you set this paramter to an array with names, the promise will
    fulfill with a hash with these names as keys and the success parameters as
    values.
    @return {Function} a function that wraps `nodeFunc` to return a `Promise`
  */
  function denodeify(nodeFunc, options) {
    var fn = function () {
      var l = arguments.length;
      var args = new Array(l + 1);
      var promiseInput = false;

      for (var i = 0; i < l; ++i) {
        var arg = arguments[i];

        if (!promiseInput) {
          // TODO: clean this up
          promiseInput = needsPromiseInput(arg);
          if (promiseInput === TRY_CATCH_ERROR) {
            var error = TRY_CATCH_ERROR.error;
            TRY_CATCH_ERROR.error = null;
            var p = new Promise(noop);
            reject(p, error);
            return p;
          } else if (promiseInput && promiseInput !== true) {
            arg = wrapThenable(promiseInput, arg);
          }
        }
        args[i] = arg;
      }

      var promise = new Promise(noop);

      args[l] = function (err, val) {
        if (err) {
          reject(promise, err);
        } else if (options === undefined) {
          resolve$1(promise, val);
        } else if (options === true) {
          resolve$1(promise, arrayResult(arguments));
        } else if (Array.isArray(options)) {
          resolve$1(promise, makeObject(arguments, options));
        } else {
          resolve$1(promise, val);
        }
      };

      if (promiseInput) {
        return handlePromiseInput(promise, args, nodeFunc, this);
      } else {
        return handleValueInput(promise, args, nodeFunc, this);
      }
    };

    fn.__proto__ = nodeFunc;

    return fn;
  }

  function handleValueInput(promise, args, nodeFunc, self) {
    var result = tryCatch(nodeFunc).apply(self, args);
    if (result === TRY_CATCH_ERROR) {
      var error = TRY_CATCH_ERROR.error;
      TRY_CATCH_ERROR.error = null;
      reject(promise, error);
    }
    return promise;
  }

  function handlePromiseInput(promise, args, nodeFunc, self) {
    return Promise.all(args).then(function (args) {
      return handleValueInput(promise, args, nodeFunc, self);
    });
  }

  function needsPromiseInput(arg) {
    if (arg !== null && typeof arg === 'object') {
      if (arg.constructor === Promise) {
        return true;
      } else {
        return getThen(arg);
      }
    } else {
      return false;
    }
  }

  /**
    This is a convenient alias for `Promise.all`.

    @method all
    @public
    @static
    @for rsvp
    @param {Array} array Array of promises.
    @param {String} [label] An optional label. This is useful
    for tooling.
  */
  function all$1(array, label) {
    return Promise.all(array, label);
  }

  function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  /**
  @module rsvp
  @public
  **/

  var AllSettled = function (_Enumerator) {
    _inherits(AllSettled, _Enumerator);

    function AllSettled(Constructor, entries, label) {
      return _possibleConstructorReturn(this, _Enumerator.call(this, Constructor, entries, false /* don't abort on reject */, label));
    }

    return AllSettled;
  }(Enumerator);

  AllSettled.prototype._setResultAt = setSettledResult;

  /**
  `RSVP.allSettled` is similar to `RSVP.all`, but instead of implementing
  a fail-fast method, it waits until all the promises have returned and
  shows you all the results. This is useful if you want to handle multiple
  promises' failure states together as a set.
   Returns a promise that is fulfilled when all the given promises have been
  settled. The return promise is fulfilled with an array of the states of
  the promises passed into the `promises` array argument.
   Each state object will either indicate fulfillment or rejection, and
  provide the corresponding value or reason. The states will take one of
  the following formats:
   ```javascript
  { state: 'fulfilled', value: value }
    or
  { state: 'rejected', reason: reason }
  ```
   Example:
   ```javascript
  let promise1 = RSVP.Promise.resolve(1);
  let promise2 = RSVP.Promise.reject(new Error('2'));
  let promise3 = RSVP.Promise.reject(new Error('3'));
  let promises = [ promise1, promise2, promise3 ];
   RSVP.allSettled(promises).then(function(array){
    // array == [
    //   { state: 'fulfilled', value: 1 },
    //   { state: 'rejected', reason: Error },
    //   { state: 'rejected', reason: Error }
    // ]
    // Note that for the second item, reason.message will be '2', and for the
    // third item, reason.message will be '3'.
  }, function(error) {
    // Not run. (This block would only be called if allSettled had failed,
    // for instance if passed an incorrect argument type.)
  });
  ```
   @method allSettled
  @public
  @static
  @for rsvp
  @param {Array} entries
  @param {String} [label] - optional string that describes the promise.
  Useful for tooling.
  @return {Promise} promise that is fulfilled with an array of the settled
  states of the constituent promises.
  */

  function allSettled(entries, label) {
    if (!Array.isArray(entries)) {
      return Promise.reject(new TypeError("Promise.allSettled must be called with an array"), label);
    }

    return new AllSettled(Promise, entries, label).promise;
  }

  /**
    This is a convenient alias for `Promise.race`.

    @method race
    @public
    @static
    @for rsvp
    @param {Array} array Array of promises.
    @param {String} [label] An optional label. This is useful
    for tooling.
   */
  function race$1(array, label) {
    return Promise.race(array, label);
  }

  function _possibleConstructorReturn$1(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits$1(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var PromiseHash = function (_Enumerator) {
    _inherits$1(PromiseHash, _Enumerator);

    function PromiseHash(Constructor, object) {
      var abortOnReject = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var label = arguments[3];
      return _possibleConstructorReturn$1(this, _Enumerator.call(this, Constructor, object, abortOnReject, label));
    }

    PromiseHash.prototype._init = function _init(Constructor, object) {
      this._result = {};
      this._enumerate(object);
    };

    PromiseHash.prototype._enumerate = function _enumerate(input) {
      var keys = Object.keys(input);

      var length = keys.length;
      var promise = this.promise;
      this._remaining = length;

      var key = void 0,
          val = void 0;
      for (var i = 0; promise._state === PENDING && i < length; i++) {
        key = keys[i];
        val = input[key];
        this._eachEntry(val, key, true);
      }

      this._checkFullfillment();
    };

    return PromiseHash;
  }(Enumerator);

  /**
    `hash` is similar to `all`, but takes an object instead of an array
    for its `promises` argument.

    Returns a promise that is fulfilled when all the given promises have been
    fulfilled, or rejected if any of them become rejected. The returned promise
    is fulfilled with a hash that has the same key names as the `promises` object
    argument. If any of the values in the object are not promises, they will
    simply be copied over to the fulfilled object.

    Example:

    ```javascript
    let promises = {
      myPromise: resolve(1),
      yourPromise: resolve(2),
      theirPromise: resolve(3),
      notAPromise: 4
    };

    hash(promises).then(function(hash){
      // hash here is an object that looks like:
      // {
      //   myPromise: 1,
      //   yourPromise: 2,
      //   theirPromise: 3,
      //   notAPromise: 4
      // }
    });
    ```

    If any of the `promises` given to `hash` are rejected, the first promise
    that is rejected will be given as the reason to the rejection handler.

    Example:

    ```javascript
    let promises = {
      myPromise: resolve(1),
      rejectedPromise: reject(new Error('rejectedPromise')),
      anotherRejectedPromise: reject(new Error('anotherRejectedPromise')),
    };

    hash(promises).then(function(hash){
      // Code here never runs because there are rejected promises!
    }, function(reason) {
      // reason.message === 'rejectedPromise'
    });
    ```

    An important note: `hash` is intended for plain JavaScript objects that
    are just a set of keys and values. `hash` will NOT preserve prototype
    chains.

    Example:

    ```javascript
    import { hash, resolve } from 'rsvp';
    function MyConstructor(){
      this.example = resolve('Example');
    }

    MyConstructor.prototype = {
      protoProperty: resolve('Proto Property')
    };

    let myObject = new MyConstructor();

    hash(myObject).then(function(hash){
      // protoProperty will not be present, instead you will just have an
      // object that looks like:
      // {
      //   example: 'Example'
      // }
      //
      // hash.hasOwnProperty('protoProperty'); // false
      // 'undefined' === typeof hash.protoProperty
    });
    ```

    @method hash
    @public
    @static
    @for rsvp
    @param {Object} object
    @param {String} [label] optional string that describes the promise.
    Useful for tooling.
    @return {Promise} promise that is fulfilled when all properties of `promises`
    have been fulfilled, or rejected if any of them become rejected.
  */
  function hash(object, label) {
    return Promise.resolve(object, label).then(function (object) {
      if (object === null || typeof object !== 'object') {
        throw new TypeError("Promise.hash must be called with an object");
      }
      return new PromiseHash(Promise, object, label).promise;
    });
  }

  function _possibleConstructorReturn$2(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits$2(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var HashSettled = function (_PromiseHash) {
    _inherits$2(HashSettled, _PromiseHash);

    function HashSettled(Constructor, object, label) {
      return _possibleConstructorReturn$2(this, _PromiseHash.call(this, Constructor, object, false, label));
    }

    return HashSettled;
  }(PromiseHash);

  HashSettled.prototype._setResultAt = setSettledResult;

  /**
    `hashSettled` is similar to `allSettled`, but takes an object
    instead of an array for its `promises` argument.

    Unlike `all` or `hash`, which implement a fail-fast method,
    but like `allSettled`, `hashSettled` waits until all the
    constituent promises have returned and then shows you all the results
    with their states and values/reasons. This is useful if you want to
    handle multiple promises' failure states together as a set.

    Returns a promise that is fulfilled when all the given promises have been
    settled, or rejected if the passed parameters are invalid.

    The returned promise is fulfilled with a hash that has the same key names as
    the `promises` object argument. If any of the values in the object are not
    promises, they will be copied over to the fulfilled object and marked with state
    'fulfilled'.

    Example:

    ```javascript
    import { hashSettled, resolve } from 'rsvp';

    let promises = {
      myPromise: resolve(1),
      yourPromise: resolve(2),
      theirPromise: resolve(3),
      notAPromise: 4
    };

    hashSettled(promises).then(function(hash){
      // hash here is an object that looks like:
      // {
      //   myPromise: { state: 'fulfilled', value: 1 },
      //   yourPromise: { state: 'fulfilled', value: 2 },
      //   theirPromise: { state: 'fulfilled', value: 3 },
      //   notAPromise: { state: 'fulfilled', value: 4 }
      // }
    });
    ```

    If any of the `promises` given to `hash` are rejected, the state will
    be set to 'rejected' and the reason for rejection provided.

    Example:

    ```javascript
    import { hashSettled, reject, resolve } from 'rsvp';

    let promises = {
      myPromise: resolve(1),
      rejectedPromise: reject(new Error('rejection')),
      anotherRejectedPromise: reject(new Error('more rejection')),
    };

    hashSettled(promises).then(function(hash){
      // hash here is an object that looks like:
      // {
      //   myPromise:              { state: 'fulfilled', value: 1 },
      //   rejectedPromise:        { state: 'rejected', reason: Error },
      //   anotherRejectedPromise: { state: 'rejected', reason: Error },
      // }
      // Note that for rejectedPromise, reason.message == 'rejection',
      // and for anotherRejectedPromise, reason.message == 'more rejection'.
    });
    ```

    An important note: `hashSettled` is intended for plain JavaScript objects that
    are just a set of keys and values. `hashSettled` will NOT preserve prototype
    chains.

    Example:

    ```javascript
    import Promise, { hashSettled, resolve } from 'rsvp';

    function MyConstructor(){
      this.example = resolve('Example');
    }

    MyConstructor.prototype = {
      protoProperty: Promise.resolve('Proto Property')
    };

    let myObject = new MyConstructor();

    hashSettled(myObject).then(function(hash){
      // protoProperty will not be present, instead you will just have an
      // object that looks like:
      // {
      //   example: { state: 'fulfilled', value: 'Example' }
      // }
      //
      // hash.hasOwnProperty('protoProperty'); // false
      // 'undefined' === typeof hash.protoProperty
    });
    ```

    @method hashSettled
    @public
    @for rsvp
    @param {Object} object
    @param {String} [label] optional string that describes the promise.
    Useful for tooling.
    @return {Promise} promise that is fulfilled when when all properties of `promises`
    have been settled.
    @static
  */

  function hashSettled(object, label) {
    return Promise.resolve(object, label).then(function (object) {
      if (object === null || typeof object !== 'object') {
        throw new TypeError("hashSettled must be called with an object");
      }

      return new HashSettled(Promise, object, false, label).promise;
    });
  }

  /**
    `rethrow` will rethrow an error on the next turn of the JavaScript event
    loop in order to aid debugging.

    Promises A+ specifies that any exceptions that occur with a promise must be
    caught by the promises implementation and bubbled to the last handler. For
    this reason, it is recommended that you always specify a second rejection
    handler function to `then`. However, `rethrow` will throw the exception
    outside of the promise, so it bubbles up to your console if in the browser,
    or domain/cause uncaught exception in Node. `rethrow` will also throw the
    error again so the error can be handled by the promise per the spec.

    ```javascript
    import { rethrow } from 'rsvp';

    function throws(){
      throw new Error('Whoops!');
    }

    let promise = new Promise(function(resolve, reject){
      throws();
    });

    promise.catch(rethrow).then(function(){
      // Code here doesn't run because the promise became rejected due to an
      // error!
    }, function (err){
      // handle the error here
    });
    ```

    The 'Whoops' error will be thrown on the next turn of the event loop
    and you can watch for it in your console. You can also handle it using a
    rejection handler given to `.then` or `.catch` on the returned promise.

    @method rethrow
    @public
    @static
    @for rsvp
    @param {Error} reason reason the promise became rejected.
    @throws Error
    @static
  */
  function rethrow(reason) {
    setTimeout(function () {
      throw reason;
    });
    throw reason;
  }

  /**
    `defer` returns an object similar to jQuery's `$.Deferred`.
    `defer` should be used when porting over code reliant on `$.Deferred`'s
    interface. New code should use the `Promise` constructor instead.

    The object returned from `defer` is a plain object with three properties:

    * promise - an `Promise`.
    * reject - a function that causes the `promise` property on this object to
      become rejected
    * resolve - a function that causes the `promise` property on this object to
      become fulfilled.

    Example:

     ```javascript
     let deferred = defer();

     deferred.resolve("Success!");

     deferred.promise.then(function(value){
       // value here is "Success!"
     });
     ```

    @method defer
    @public
    @static
    @for rsvp
    @param {String} [label] optional string for labeling the promise.
    Useful for tooling.
    @return {Object}
   */

  function defer(label) {
    var deferred = { resolve: undefined, reject: undefined };

    deferred.promise = new Promise(function (resolve, reject) {
      deferred.resolve = resolve;
      deferred.reject = reject;
    }, label);

    return deferred;
  }

  function _possibleConstructorReturn$3(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits$3(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var MapEnumerator = function (_Enumerator) {
    _inherits$3(MapEnumerator, _Enumerator);

    function MapEnumerator(Constructor, entries, mapFn, label) {
      return _possibleConstructorReturn$3(this, _Enumerator.call(this, Constructor, entries, true, label, mapFn));
    }

    MapEnumerator.prototype._init = function _init(Constructor, input, bool, label, mapFn) {
      var len = input.length || 0;
      this.length = len;
      this._remaining = len;
      this._result = new Array(len);
      this._mapFn = mapFn;

      this._enumerate(input);
    };

    MapEnumerator.prototype._setResultAt = function _setResultAt(state, i, value, firstPass) {
      if (firstPass) {
        var val = tryCatch(this._mapFn)(value, i);
        if (val === TRY_CATCH_ERROR) {
          this._settledAt(REJECTED, i, val.error, false);
        } else {
          this._eachEntry(val, i, false);
        }
      } else {
        this._remaining--;
        this._result[i] = value;
      }
    };

    return MapEnumerator;
  }(Enumerator);

  /**
   `map` is similar to JavaScript's native `map` method. `mapFn` is eagerly called
    meaning that as soon as any promise resolves its value will be passed to `mapFn`.
    `map` returns a promise that will become fulfilled with the result of running
    `mapFn` on the values the promises become fulfilled with.

    For example:

    ```javascript
    import { map, resolve } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = resolve(2);
    let promise3 = resolve(3);
    let promises = [ promise1, promise2, promise3 ];

    let mapFn = function(item){
      return item + 1;
    };

    map(promises, mapFn).then(function(result){
      // result is [ 2, 3, 4 ]
    });
    ```

    If any of the `promises` given to `map` are rejected, the first promise
    that is rejected will be given as an argument to the returned promise's
    rejection handler. For example:

    ```javascript
    import { map, reject, resolve } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = reject(new Error('2'));
    let promise3 = reject(new Error('3'));
    let promises = [ promise1, promise2, promise3 ];

    let mapFn = function(item){
      return item + 1;
    };

    map(promises, mapFn).then(function(array){
      // Code here never runs because there are rejected promises!
    }, function(reason) {
      // reason.message === '2'
    });
    ```

    `map` will also wait if a promise is returned from `mapFn`. For example,
    say you want to get all comments from a set of blog posts, but you need
    the blog posts first because they contain a url to those comments.

    ```javscript
    import { map } from 'rsvp';

    let mapFn = function(blogPost){
      // getComments does some ajax and returns an Promise that is fulfilled
      // with some comments data
      return getComments(blogPost.comments_url);
    };

    // getBlogPosts does some ajax and returns an Promise that is fulfilled
    // with some blog post data
    map(getBlogPosts(), mapFn).then(function(comments){
      // comments is the result of asking the server for the comments
      // of all blog posts returned from getBlogPosts()
    });
    ```

    @method map
    @public
    @static
    @for rsvp
    @param {Array} promises
    @param {Function} mapFn function to be called on each fulfilled promise.
    @param {String} [label] optional string for labeling the promise.
    Useful for tooling.
    @return {Promise} promise that is fulfilled with the result of calling
    `mapFn` on each fulfilled promise or value when they become fulfilled.
     The promise will be rejected if any of the given `promises` become rejected.
  */
  function map(promises, mapFn, label) {
    if (typeof mapFn !== 'function') {
      return Promise.reject(new TypeError("map expects a function as a second argument"), label);
    }

    return Promise.resolve(promises, label).then(function (promises) {
      if (!Array.isArray(promises)) {
        throw new TypeError("map must be called with an array");
      }
      return new MapEnumerator(Promise, promises, mapFn, label).promise;
    });
  }

  /**
    This is a convenient alias for `Promise.resolve`.

    @method resolve
    @public
    @static
    @for rsvp
    @param {*} value value that the returned promise will be resolved with
    @param {String} [label] optional string for identifying the returned promise.
    Useful for tooling.
    @return {Promise} a promise that will become fulfilled with the given
    `value`
  */
  function resolve$2(value, label) {
    return Promise.resolve(value, label);
  }

  /**
    This is a convenient alias for `Promise.reject`.

    @method reject
    @public
    @static
    @for rsvp
    @param {*} reason value that the returned promise will be rejected with.
    @param {String} [label] optional string for identifying the returned promise.
    Useful for tooling.
    @return {Promise} a promise rejected with the given `reason`.
  */
  function reject$2(reason, label) {
    return Promise.reject(reason, label);
  }

  function _possibleConstructorReturn$4(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits$4(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var EMPTY_OBJECT = {};

  var FilterEnumerator = function (_MapEnumerator) {
    _inherits$4(FilterEnumerator, _MapEnumerator);

    function FilterEnumerator() {
      return _possibleConstructorReturn$4(this, _MapEnumerator.apply(this, arguments));
    }

    FilterEnumerator.prototype._checkFullfillment = function _checkFullfillment() {
      if (this._remaining === 0 && this._result !== null) {
        var result = this._result.filter(function (val) {
          return val !== EMPTY_OBJECT;
        });
        fulfill(this.promise, result);
        this._result = null;
      }
    };

    FilterEnumerator.prototype._setResultAt = function _setResultAt(state, i, value, firstPass) {
      if (firstPass) {
        this._result[i] = value;
        var val = tryCatch(this._mapFn)(value, i);
        if (val === TRY_CATCH_ERROR) {
          this._settledAt(REJECTED, i, val.error, false);
        } else {
          this._eachEntry(val, i, false);
        }
      } else {
        this._remaining--;
        if (!value) {
          this._result[i] = EMPTY_OBJECT;
        }
      }
    };

    return FilterEnumerator;
  }(MapEnumerator);

  /**
   `filter` is similar to JavaScript's native `filter` method.
   `filterFn` is eagerly called meaning that as soon as any promise
    resolves its value will be passed to `filterFn`. `filter` returns
    a promise that will become fulfilled with the result of running
    `filterFn` on the values the promises become fulfilled with.

    For example:

    ```javascript
    import { filter, resolve } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = resolve(2);
    let promise3 = resolve(3);

    let promises = [promise1, promise2, promise3];

    let filterFn = function(item){
      return item > 1;
    };

    filter(promises, filterFn).then(function(result){
      // result is [ 2, 3 ]
    });
    ```

    If any of the `promises` given to `filter` are rejected, the first promise
    that is rejected will be given as an argument to the returned promise's
    rejection handler. For example:

    ```javascript
    import { filter, reject, resolve } from 'rsvp';

    let promise1 = resolve(1);
    let promise2 = reject(new Error('2'));
    let promise3 = reject(new Error('3'));
    let promises = [ promise1, promise2, promise3 ];

    let filterFn = function(item){
      return item > 1;
    };

    filter(promises, filterFn).then(function(array){
      // Code here never runs because there are rejected promises!
    }, function(reason) {
      // reason.message === '2'
    });
    ```

    `filter` will also wait for any promises returned from `filterFn`.
    For instance, you may want to fetch a list of users then return a subset
    of those users based on some asynchronous operation:

    ```javascript
    import { filter, resolve } from 'rsvp';

    let alice = { name: 'alice' };
    let bob   = { name: 'bob' };
    let users = [ alice, bob ];

    let promises = users.map(function(user){
      return resolve(user);
    });

    let filterFn = function(user){
      // Here, Alice has permissions to create a blog post, but Bob does not.
      return getPrivilegesForUser(user).then(function(privs){
        return privs.can_create_blog_post === true;
      });
    };
    filter(promises, filterFn).then(function(users){
      // true, because the server told us only Alice can create a blog post.
      users.length === 1;
      // false, because Alice is the only user present in `users`
      users[0] === bob;
    });
    ```

    @method filter
    @public
    @static
    @for rsvp
    @param {Array} promises
    @param {Function} filterFn - function to be called on each resolved value to
    filter the final results.
    @param {String} [label] optional string describing the promise. Useful for
    tooling.
    @return {Promise}
  */

  function filter(promises, filterFn, label) {
    if (typeof filterFn !== 'function') {
      return Promise.reject(new TypeError("filter expects function as a second argument"), label);
    }

    return Promise.resolve(promises, label).then(function (promises) {
      if (!Array.isArray(promises)) {
        throw new TypeError("filter must be called with an array");
      }
      return new FilterEnumerator(Promise, promises, filterFn, label).promise;
    });
  }

  var len = 0;
  var vertxNext = void 0;
  function asap(callback, arg) {
    queue$1[len] = callback;
    queue$1[len + 1] = arg;
    len += 2;
    if (len === 2) {
      // If len is 1, that means that we need to schedule an async flush.
      // If additional callbacks are queued before the queue is flushed, they
      // will be processed by this flush that we are scheduling.
      scheduleFlush$1();
    }
  }

  var browserWindow = typeof window !== 'undefined' ? window : undefined;
  var browserGlobal = browserWindow || {};
  var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
  var isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

  // test for web worker but not in IE10
  var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';

  // node
  function useNextTick() {
    var nextTick = process.nextTick;
    // node version 0.10.x displays a deprecation warning when nextTick is used recursively
    // setImmediate should be used instead instead
    var version = process.versions.node.match(/^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/);
    if (Array.isArray(version) && version[1] === '0' && version[2] === '10') {
      nextTick = setImmediate;
    }
    return function () {
      return nextTick(flush);
    };
  }

  // vertx
  function useVertxTimer() {
    if (typeof vertxNext !== 'undefined') {
      return function () {
        vertxNext(flush);
      };
    }
    return useSetTimeout();
  }

  function useMutationObserver() {
    var iterations = 0;
    var observer = new BrowserMutationObserver(flush);
    var node = document.createTextNode('');
    observer.observe(node, { characterData: true });

    return function () {
      return node.data = iterations = ++iterations % 2;
    };
  }

  // web worker
  function useMessageChannel() {
    var channel = new MessageChannel();
    channel.port1.onmessage = flush;
    return function () {
      return channel.port2.postMessage(0);
    };
  }

  function useSetTimeout() {
    return function () {
      return setTimeout(flush, 1);
    };
  }

  var queue$1 = new Array(1000);

  function flush() {
    for (var i = 0; i < len; i += 2) {
      var callback = queue$1[i];
      var arg = queue$1[i + 1];

      callback(arg);

      queue$1[i] = undefined;
      queue$1[i + 1] = undefined;
    }

    len = 0;
  }

  function attemptVertex() {
    try {
      var vertx = Function('return this')().require('vertx');
      vertxNext = vertx.runOnLoop || vertx.runOnContext;
      return useVertxTimer();
    } catch (e) {
      return useSetTimeout();
    }
  }

  var scheduleFlush$1 = void 0;
  // Decide what async method to use to triggering processing of queued callbacks:
  if (isNode) {
    scheduleFlush$1 = useNextTick();
  } else if (BrowserMutationObserver) {
    scheduleFlush$1 = useMutationObserver();
  } else if (isWorker) {
    scheduleFlush$1 = useMessageChannel();
  } else if (browserWindow === undefined && typeof require === 'function') {
    scheduleFlush$1 = attemptVertex();
  } else {
    scheduleFlush$1 = useSetTimeout();
  }

  // defaults
  config.async = asap;
  config.after = function (cb) {
    return setTimeout(cb, 0);
  };
  var cast = resolve$2;

  var async = function (callback, arg) {
    return config.async(callback, arg);
  };

  function on() {
    config.on.apply(config, arguments);
  }

  function off() {
    config.off.apply(config, arguments);
  }

  // Set up instrumentation through `window.__PROMISE_INTRUMENTATION__`
  if (typeof window !== 'undefined' && typeof window['__PROMISE_INSTRUMENTATION__'] === 'object') {
    var callbacks = window['__PROMISE_INSTRUMENTATION__'];
    configure('instrument', true);
    for (var eventName in callbacks) {
      if (callbacks.hasOwnProperty(eventName)) {
        on(eventName, callbacks[eventName]);
      }
    }
  }

  // the default export here is for backwards compat:
  //   https://github.com/tildeio/rsvp.js/issues/434
  var rsvp = {
    asap: asap,
    cast: cast,
    Promise: Promise,
    EventTarget: EventTarget,
    all: all$1,
    allSettled: allSettled,
    race: race$1,
    hash: hash,
    hashSettled: hashSettled,
    rethrow: rethrow,
    defer: defer,
    denodeify: denodeify,
    configure: configure,
    on: on,
    off: off,
    resolve: resolve$2,
    reject: reject$2,
    map: map,
    async: async,
    filter: filter
  };

  exports.default = rsvp;
  exports.asap = asap;
  exports.cast = cast;
  exports.Promise = Promise;
  exports.EventTarget = EventTarget;
  exports.all = all$1;
  exports.allSettled = allSettled;
  exports.race = race$1;
  exports.hash = hash;
  exports.hashSettled = hashSettled;
  exports.rethrow = rethrow;
  exports.defer = defer;
  exports.denodeify = denodeify;
  exports.configure = configure;
  exports.on = on;
  exports.off = off;
  exports.resolve = resolve$2;
  exports.reject = reject$2;
  exports.map = map;
  exports.async = async;
  exports.filter = filter;

  Object.defineProperty(exports, '__esModule', { value: true });

})));





//# sourceMappingURL=rsvp.map

"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class Loader
 *
 * @description 
 *  Responsibilities
 *  > Being a facade for an ajax request of contents
 *
 *  @method load(url)
 *  @param {object} args = {
 * 
 *      @param    {string} worker  - the full path name of the worker
 *      @callback onLoading        - called when the loading is true
 *      @callback onLoad(payload)  - called when the loading is complete
 *      @callback onError(error)   - called when the loading is failed
 *  }
 * 
 *  @property {boolean} loading - state of the loader
 */
var Loader = /*#__PURE__*/function () {
  function Loader(args) {
    _classCallCheck(this, Loader);

    // state of the loader
    this.loading = false; // instantiate the Web Worker (parallel thread), if possible
    //this.worker    = (typeof args != 'undefined' && typeof args.worker != 'undefined') ? args.worker : this._setWorker(),
    //this.loader    = this._setLoaderWorker();

    this.loader = null;
    this.loader = this._setWorker();
    this.onLoad = typeof args != 'undefined' && args.onLoad ? args.onLoad : null;
    this.onError = typeof args != 'undefined' && args.onError ? args.onError : null;
    this.onLoading = typeof args != 'undefined' && args.onLoading ? args.onLoading : null; //console.log("this.worker LOADER: ",this.worker);
  }
  /* 
   * makes some adjustments to adapt the worker path
   * different dev environments
   */


  _createClass(Loader, [{
    key: "_setLoaderWorker",
    value: function _setLoaderWorker() {
      try {
        if (window.Worker) return new Worker(this.worker);
      } catch (err) {
        console.log('_b| [loader] ', err.message);
      }
    }
  }, {
    key: "_loading",
    value: function _loading() {
      // sets the status of loading 
      this.loading = true; // launch the callback

      if (this.onLoading) this.onLoading();
    }
  }, {
    key: "_error",
    value: function _error(error) {
      // resets the state
      this.loading = false; // launch the callback  

      if (this.onError) this.onError(error);
    }
  }, {
    key: "_loaded",
    value: function _loaded(payload) {
      // resets the state
      this.loading = false; // launch the callback

      if (this.onLoad) this.onLoad(payload);
    }
    /**
     * Execute loading. In a new thread, if possible.
     * takes as a parameter the url to load from and three callbacks
     * to update the state
     */

  }, {
    key: "_parallelLoad",
    value: function _parallelLoad(url) {
      var load_self = this;

      if (this.loader) {
        // dispatch of the incoming messages            
        this.loader.onmessage = function (msg) {
          switch (msg.data.type) {
            case 'loaded':
              load_self._loaded.call(load_self, msg.data.payload);

              break;

            case 'loading':
              load_self._loading.call(load_self);

              break;

            case 'error':
              load_self._error.call(load_self);

              break;
          }
        }; // send the url to be loaded to the loader thread


        this.loader.postMessage({
          url: url
        });
      } else {
        console.log('_b| [loader] not threaded'); // IE9 version not parallel at all

        var oReq = new XMLHttpRequest();

        oReq.onload = function (response) {
          var payload = this.responseText;

          load_self._loaded.call(load_self, payload);
        };

        oReq.onerror = function () {
          load_self._error.call(load_self);
        };

        oReq.open("GET", url);
        oReq.send();

        load_self._loading.call(load_self);
      }
    }
    /**
     * loads the content from the given url
     */

  }, {
    key: "load",
    value: function load(url) {
      if (url != null) this._parallelLoad(url);
    }
  }, {
    key: "_setWorker",
    value: function _setWorker() {
      var content = [" onmessage = function(msg) { if(msg.data.url) load(msg.data.url); else postMessage({type : 'error', text : 'no url specified'}); }\n" + " load = function(url) { var oReq = new XMLHttpRequest(); oReq.onload = transferComplete; oReq.onerror = transferFailed; console.log('_b| loading...', url); oReq.open('GET', url); oReq.send(); postMessage({type : 'loading'}); }\n" + " function transferComplete() { console.log('_b| loaded'/*, this*/); postMessage({type: 'loaded', payload : this.response}); }\n" + " function transferFailed() { console.log('_b| load error', this.responseText); postMessage({type: 'error', text : this.response}); }\n"];
      var blob = new Blob(content);
      return new Worker(window.URL.createObjectURL(blob));
    }
  }]);

  return Loader;
}();
/*!
* basket.js
* v0.5.2 - 2015-02-07
* http://addyosmani.github.com/basket.js
* (c) Addy Osmani;  License
* Created by: Addy Osmani, Sindre Sorhus, Andrée Hansson, Mat Scales
* Contributors: Ironsjp, Mathias Bynens, Rick Waldron, Felipe Morais
* Uses rsvp.js, https://github.com/tildeio/rsvp.js
*/
!function(a,b){"use strict";var c=b.head||b.getElementsByTagName("head")[0],d="basket-",e=5e3,f=[],g=function(a,b){try{return localStorage.setItem(d+a,JSON.stringify(b)),!0}catch(c){if(c.name.toUpperCase().indexOf("QUOTA")>=0){var e,f=[];for(e in localStorage)0===e.indexOf(d)&&f.push(JSON.parse(localStorage[e]));return f.length?(f.sort(function(a,b){return a.stamp-b.stamp}),basket.remove(f[0].key),g(a,b)):void 0}return}},h=function(a){var b=new RSVP.Promise(function(b,c){var d=new XMLHttpRequest;d.open("GET",a),d.onreadystatechange=function(){4===d.readyState&&(200===d.status||0===d.status&&d.responseText?b({content:d.responseText,type:d.getResponseHeader("content-type")}):c(new Error(d.statusText)))},setTimeout(function(){d.readyState<4&&d.abort()},basket.timeout),d.send()});return b},i=function(a){return h(a.url).then(function(b){var c=j(a,b);return a.skipCache||g(a.key,c),c})},j=function(a,b){var c=+new Date;return a.data=b.content,a.originalType=b.type,a.type=a.type||b.type,a.skipCache=a.skipCache||!1,a.stamp=c,a.expire=c+60*(a.expire||e)*60*1e3,a},k=function(a,b){return!a||a.expire-+new Date<0||b.unique!==a.unique||basket.isValidItem&&!basket.isValidItem(a,b)},l=function(a){var b,c,d;if(a.url)return a.key=a.key||a.url,b=basket.get(a.key),a.execute=a.execute!==!1,d=k(b,a),a.live||d?(a.unique&&(a.url+=(a.url.indexOf("?")>0?"&":"?")+"basket-unique="+a.unique),c=i(a),a.live&&!d&&(c=c.then(function(a){return a},function(){return b}))):(b.type=a.type||b.originalType,b.execute=a.execute,c=new RSVP.Promise(function(a){a(b)})),c},m=function(a){var d=b.createElement("script");d.defer=!0,d.text=a.data,c.appendChild(d)},n={"default":m},o=function(a){return a.type&&n[a.type]?n[a.type](a):n["default"](a)},p=function(a){return a.map(function(a){return a.execute&&o(a),a})},q=function(){var a,b,c=[];for(a=0,b=arguments.length;b>a;a++)c.push(l(arguments[a]));return RSVP.all(c)},r=function(){var a=q.apply(null,arguments),b=this.then(function(){return a}).then(p);return b.thenRequire=r,b};a.basket={require:function(){for(var a=0,b=arguments.length;b>a;a++)arguments[a].execute=arguments[a].execute!==!1,arguments[a].once&&f.indexOf(arguments[a].url)>=0?arguments[a].execute=!1:arguments[a].execute!==!1&&f.indexOf(arguments[a].url)<0&&f.push(arguments[a].url);var c=q.apply(null,arguments).then(p);return c.thenRequire=r,c},remove:function(a){return localStorage.removeItem(d+a),this},get:function(a){var b=localStorage.getItem(d+a);try{return JSON.parse(b||"false")}catch(c){return!1}},clear:function(a){var b,c,e=+new Date;for(b in localStorage)c=b.split(d)[1],c&&(!a||this.get(c).expire<=e)&&this.remove(c);return this},isValidItem:null,timeout:5e3,addHandler:function(a,b){Array.isArray(a)||(a=[a]),a.forEach(function(a){n[a]=b})},removeHandler:function(a){basket.addHandler(a,void 0)}},basket.clear(!0)}(this,document);
//# sourceMappingURL=basket.min.js.map
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class DateFormat
 * 
 * @description
 *  change the date labels in a more elegant way
 * 
 * @param {string} label_selector - the selector used for the label "published"
 * @param {string} time_selector  - the selector used 
 */
var DateFormat = /*#__PURE__*/function () {
  function DateFormat() {
    var time_selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '.time';
    var label_selector = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '.label-time';

    _classCallCheck(this, DateFormat);

    this.months = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
    this.ranges = {
      yesterday: [{
        start: 0,
        end: 12,
        text: 'Ieri mattina'
      }, {
        start: 13,
        end: 17,
        text: 'Ieri pomeriggio'
      }, {
        start: 17,
        end: 24,
        text: 'Ieri sera'
      }]
    };
    this.label_selector = label_selector;
    this.time_selector = time_selector;
  }

  _createClass(DateFormat, [{
    key: "_daysPast",
    value: function _daysPast(date) {
      var now = new Date();
      return Math.floor((Date.UTC(now.getFullYear(), now.getMonth(), now.getDate()) - Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())) / (1000 * 60 * 60 * 24));
    } // given the date returns the string 
    // corresponding to the day moment

  }, {
    key: "_getDayMoment",
    value: function _getDayMoment(date) {
      var h = date.getHours(),
          m = date.getMinutes();

      for (var i = 0; i < this.ranges.yesterday.length; i++) {
        if (h > this.ranges.yesterday[i].start && h < this.ranges.yesterday[i].end) return this.ranges.yesterday[i].text;
      }
    }
  }, {
    key: "_getHoursPast",
    value: function _getHoursPast(date, seconds) {
      var interval = Math.floor(seconds / 3600);
      if (interval > 0 && interval <= 3) return interval + (interval == 1 ? " ora" : " ore") + " fa";
      interval = Math.floor(seconds / 60);
      if (interval > 1 && interval < 60) return interval + ' minuti fa';else return 'Oggi alle ' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    }
  }, {
    key: "_getBasicFormat",
    value: function _getBasicFormat(date) {
      var currentYear = new Date().getFullYear();
      var day = date.getDate();
      var month = date.getMonth();
      var year = date.getFullYear(); //day = day < 10 ? '0'+day : day;  

      return day + ' ' + this.months[month] + (currentYear > year ? ' ' + year : '');
    }
  }, {
    key: "_setTimeLabels",
    value: function _setTimeLabels(timestamp) {
      var date = new Date(timestamp * 1000);
      var now = new Date();
      var seconds = Math.floor((now - date) / 1000);
      var value = {
        label: '',
        time: ''
      };

      var days = this._daysPast(date);

      value.label = days >= 0 && days <= 3 ? 'Pubblicato' : 'Pubblicato il';

      switch (days) {
        case 0:
          value.time = this._getHoursPast(date, seconds);
          break;

        case 1:
          value.time = this._getDayMoment(date);
          break;

        case 2:
          value.time = '2 giorni fa';
          break;

        case 3:
          value.time = '3 giorni fa';
          break;

        default:
          value.time = this._getBasicFormat(date);
      }

      console.log('_d| _setTimeLabels()', value);
      return value;
    }
  }, {
    key: "format",
    value: function format() {
      var times = document.querySelectorAll(this.time_selector);
      var labels = document.querySelectorAll(this.label_selector);
      console.log('_d| times, labels', times, labels);
      if (times.length == labels.length) for (var i = 0; i < times.length; i++) {
        var value = this._setTimeLabels(times[i].dataset.timestamp);

        times[i].textContent = value.time;
        labels[i].textContent = value.label;
      } else console.log('[DateFormat] il numero di labels non è uguale a quello delle date\n', 'date:', times.length, 'labels', labels.length);
    }
  }]);

  return DateFormat;
}();
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class UIActions
 * 
 * @description
 *  Responsibility:
 *  > Set some behaviours to be applied on most pages,
 *    commons for the whole layout and in the ATF
 * 
 * @dependencies
 *  Scroller
 * 
 */
var UIActions = /*#__PURE__*/function () {
  /**
   * Dependencies: Scroller, InfiniteScroll, DateFormat
   * 
   * @param {object} args {
   *   @property {Scroller} scroller - a reference to a Scroller instance 
   *   @property {InfiniteScroll} infinite - a reference to a InfiniteScroll instance 
   * }
   */
  function UIActions(args) {
    _classCallCheck(this, UIActions);

    // 1. get the main objects
    this.scroller = typeof args.scroller !== 'undefined' ? args.scroller : null; //new Scroller();
    //this.infinite = (typeof args.infinite !== 'undefined') ? args.infinite : null; 
    // provides a more elegant representation for the dates

    this.date = new DateFormat(); // applies the correct format to the dates

    this.date.format();
    var that = this; // after each page rendering

    /*if(this.infinite)
        this.infinite.loader.loadCallbackStack.push( function() {
            that.date.format.call(that.date);
        });*/
    // activate modal buttons

    /*         let modal = document.querySelector(".modal");
            if(typeof modal != 'undefined') this.setOpenCloseModal(modal); */
    // this.wrapper = document.querySelector(".main-wrapper");
    // this.navbar  = document.querySelector(".navbar");
    // this.skins_offset = 100;
    //this.setMenuButtons();
    //this.checkSkinEnabled();
    //this.setTopBarScrollBehaviour.bind(this.setTopBarScrollBehaviour)
    //if(this.wrapper) this.scroller.actionStack.push(this.setTopBarScrollBehaviour);
  }

  _createClass(UIActions, [{
    key: "checkSkinEnabled",
    value: function checkSkinEnabled() {
      /* wrong this.html !!! */

      /*
      if(this.html.classList.contains('skin-enabled')) {
          this.main_header.classList.remove('fixed');
          this.shrinkY = 109;
          this.skin = true;
          this.loadProductTriggerDistance = 50;
      }
      */
    }
  }, {
    key: "setTopBarScrollBehaviour",
    value: function setTopBarScrollBehaviour() {
      if (this.scroller.viewPortPosition > this.skins_offset) {
        this.wrapper.classList.add("scroll-offset");
        this.navbar.classList.add("scroll-offset", "scrolled");
      } else {
        this.wrapper.classList.remove("scroll-offset");
        this.navbar.classList.remove("scroll-offset", "scrolled");
      }
    }
    /**
     * Sets actions for menu buttons
     */

  }, {
    key: "setMenuButtons",
    value: function setMenuButtons() {
      var menu_button = document.querySelector(".menu-icon");
      var menu_panel = document.querySelector(".menu-panel");
      var close_search = document.querySelector('.search-panel');
      var close_searchicon = document.querySelector('.open-search');
      var mobile_handle = document.querySelector('.mobile-handle');
      var topbtn_hide = document.getElementById('topBtn');
      var strip_menu = document.querySelector('.category-menu.desktop');

      menu_button.onclick = function () {
        menu_button.classList.toggle("isOpen"); // is toggle supported on ie9?

        menu_panel.classList.toggle("open");
        document.body.classList.toggle("noscroll");
        close_search.classList.remove("open");

        if (close_searchicon.innerHTML === '<i class="icon-plus rotate"></i>') {
          close_searchicon.innerHTML = '<i class="icon-search"></i>';
        }

        if (menu_panel.classList.contains('open')) {
          topbtn_hide.classList.add('hidden');
          strip_menu.classList.add('hidden');
        } else {
          topbtn_hide.classList.remove('hidden');
          strip_menu.classList.remove('hidden');
        }
      };
    }
    /**
     * Sets actions for modal's buttons
     */

    /*     setOpenCloseModal(_modal) {
            const toggle = document.querySelector(".toggle-modal");
            const closemodal = document.querySelector(".closemodal");
            const modal = _modal;
         
            toggle.addEventListener("click", function() {
                modal.classList.toggle("active");
            });
         
            closemodal.addEventListener("click", function() {
                modal.classList.remove("active");
            });
        } */

  }]);

  return UIActions;
}();
!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t=t||self).LazyLoad=n()}(this,(function(){"use strict";function t(){return(t=Object.assign||function(t){for(var n=1;n<arguments.length;n++){var e=arguments[n];for(var o in e)Object.prototype.hasOwnProperty.call(e,o)&&(t[o]=e[o])}return t}).apply(this,arguments)}var n="undefined"!=typeof window,e=n&&!("onscroll"in window)||"undefined"!=typeof navigator&&/(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),o=n&&"IntersectionObserver"in window,a=n&&"classList"in document.createElement("p"),r={elements_selector:"img",container:e||n?document:null,threshold:300,thresholds:null,data_src:"src",data_srcset:"srcset",data_sizes:"sizes",data_bg:"bg",data_poster:"poster",class_loading:"loading",class_loaded:"loaded",class_error:"error",load_delay:0,auto_unobserve:!0,callback_enter:null,callback_exit:null,callback_loading:null,callback_loaded:null,callback_error:null,callback_finish:null,use_native:!1},i=function(n){return t({},r,n)},s=function(t,n){var e,o=new t(n);try{e=new CustomEvent("LazyLoad::Initialized",{detail:{instance:o}})}catch(t){(e=document.createEvent("CustomEvent")).initCustomEvent("LazyLoad::Initialized",!1,!1,{instance:o})}window.dispatchEvent(e)},c=function(t,n){return t.getAttribute("data-"+n)},l=function(t,n,e){var o="data-"+n;null!==e?t.setAttribute(o,e):t.removeAttribute(o)},u=function(t,n){return l(t,"ll-status",n)},d=function(t,n){return l(t,"ll-timeout",n)},f=function(t){return c(t,"ll-timeout")},_=function(t){for(var n,e=[],o=0;n=t.children[o];o+=1)"SOURCE"===n.tagName&&e.push(n);return e},v=function(t,n,e){e&&t.setAttribute(n,e)},g=function(t,n){v(t,"sizes",c(t,n.data_sizes)),v(t,"srcset",c(t,n.data_srcset)),v(t,"src",c(t,n.data_src))},h={IMG:function(t,n){var e=t.parentNode;e&&"PICTURE"===e.tagName&&_(e).forEach((function(t){g(t,n)}));g(t,n)},IFRAME:function(t,n){v(t,"src",c(t,n.data_src))},VIDEO:function(t,n){_(t).forEach((function(t){v(t,"src",c(t,n.data_src))})),v(t,"poster",c(t,n.data_poster)),v(t,"src",c(t,n.data_src)),t.load()}},b=function(t,n,e){var o=t.tagName,a=h[o];a?(a(t,n),function(t){t&&(t.loadingCount+=1)}(e)):function(t,n){var e=c(t,n.data_src),o=c(t,n.data_bg);e&&(t.style.backgroundImage='url("'.concat(e,'")')),o&&(t.style.backgroundImage=o)}(t,n)},p=function(t,n){a?t.classList.add(n):t.className+=(t.className?" ":"")+n},m=function(t,n){a?t.classList.remove(n):t.className=t.className.replace(new RegExp("(^|\\s+)"+n+"(\\s+|$)")," ").replace(/^\s+/,"").replace(/\s+$/,"")},y=function(t,n,e,o){t&&(void 0===o?void 0===e?t(n):t(n,e):t(n,e,o))},E=function(t,n){n&&(n.loadingCount-=1,L(t,n))},L=function(t,n){n.toLoadCount||n.loadingCount||y(t.callback_finish,n)},w=function(t,n,e){t.addEventListener(n,e)},I=function(t,n,e){t.removeEventListener(n,e)},k=function(t,n,e){I(t,"load",n),I(t,"loadeddata",n),I(t,"error",e)},C=function(t,n,e){var o=function o(r){!function(t,n,e){var o=t.target;u(o,"loaded"),m(o,n.class_loading),p(o,n.class_loaded),y(n.callback_loaded,o,e),E(n,e)}(r,n,e),k(t,o,a)},a=function a(r){!function(t,n,e){var o=t.target;u(o,"error"),m(o,n.class_loading),p(o,n.class_error),y(n.callback_error,o,e),E(n,e)}(r,n,e),k(t,o,a)};!function(t,n,e){w(t,"load",n),w(t,"loadeddata",n),w(t,"error",e)}(t,o,a)},A=["IMG","IFRAME","VIDEO"],z=function(t,n,e){(function(t){return A.indexOf(t.tagName)>-1})(t)&&(C(t,n,e),p(t,n.class_loading)),b(t,n,e),function(t,n){n&&(n.toLoadCount-=1,L(t,n))}(n,e)},O=function(t,n,e){z(t,n,e),u(t,"loading"),y(n.callback_loading,t,e),y(n.callback_reveal,t,e),function(t,n){if(n){var e=n._observer;e&&n._settings.auto_unobserve&&e.unobserve(t)}}(t,e)},N=function(t){var n=f(t);n&&(clearTimeout(n),d(t,null))},x=function(t,n,e){var o=e._settings;y(o.callback_enter,t,n,e),o.load_delay?function(t,n,e){var o=n.load_delay,a=f(t);a||(a=setTimeout((function(){O(t,n,e),N(t)}),o),d(t,a))}(t,o,e):O(t,o,e)},M=["IMG","IFRAME"],R=function(t){return t.use_native&&"loading"in HTMLImageElement.prototype},T=function(t,n,e){t.forEach((function(t){-1!==M.indexOf(t.tagName)&&(t.setAttribute("loading","lazy"),function(t,n,e){z(t,n,e),u(t,"native")}(t,n,e))})),e.toLoadCount=0},j=function(t,n){!function(t){t.disconnect()}(t),function(t,n){n.forEach((function(n){t.observe(n),u(n,"observed")}))}(t,n)},F=function(t){var n;o&&!R(t._settings)&&(t._observer=new IntersectionObserver((function(n){n.forEach((function(n){return function(t){return t.isIntersecting||t.intersectionRatio>0}(n)?x(n.target,n,t):function(t,n,e){var o=e._settings;y(o.callback_exit,t,n,e),o.load_delay&&N(t)}(n.target,n,t)}))}),{root:(n=t._settings).container===document?null:n.container,rootMargin:n.thresholds||n.threshold+"px"}))},G=function(t){return Array.prototype.slice.call(t)},D=function(t){return t.container.querySelectorAll(t.elements_selector)},P=function(t){return!function(t){return null!==c(t,"ll-status")}(t)||function(t){return"observed"===c(t,"ll-status")}(t)},S=function(t){return function(t){return"error"===c(t,"ll-status")}(t)},U=function(t,n){return function(t){return G(t).filter(P)}(t||D(n))},V=function(t){var n,e=t._settings;(n=D(e),G(n).filter(S)).forEach((function(t){m(t,e.class_error),function(t){l(t,"ll-status",null)}(t)})),t.update()},$=function(t,e){var o;this._settings=i(t),this.loadingCount=0,F(this),o=this,n&&window.addEventListener("online",(function(t){V(o)})),this.update(e)};return $.prototype={update:function(t){var n=this._settings,a=U(t,n);this.toLoadCount=a.length,!e&&o?R(n)?T(a,n,this):j(this._observer,a):this.loadAll(a)},destroy:function(){this._observer&&this._observer.disconnect(),delete this._observer,delete this._settings,delete this.loadingCount,delete this.toLoadCount},loadAll:function(t){var n=this,e=this._settings;U(t,e).forEach((function(t){O(t,e,n)}))},load:function(t){O(t,this._settings,this)}},$.load=function(t,n){var e=i(n);O(t,e)},n&&function(t,n){if(n)if(n.length)for(var e,o=0;e=n[o];o+=1)s(t,e);else s(t,n)}($,window.lazyLoadOptions),$}));

"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class Tracker
 * 
 * @responsibility
 * track Google Analytics events of the given element list
 * keep count of the already tracked urls for each page and avoids tracking 
 * an url more than once for each page
 * 
 * allows the pageview tracking as well
 *  
 * @dependency
 * Scroller, gtag
 * 
 * @param {Object} args {
 *      selectors :  [ { selector : "string", type: "event|pageview", name : "name", args : {event_category, event_label, value } },  ... ],
 *      scroller : {{ Scroller Object }}
 *      gaid : 'GA_MEASUREMENT_ID'
 * }
 * 
 * @tutorial https://developers.google.com/analytics/devguides/collection/gtagjs/sending-data
 * 
 *  FANTASTICO IL MODO DI CREARE GRUPPI SU ANALYTICS
 *  FANTASTICO IL FATTO CHE SI POSSONO FARE DELLE EVENT_CALLBACK
 */
var Tracker = /*#__PURE__*/function () {
  function Tracker() {
    var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Tracker);

    // array of selectors to track views and clicks
    this.selectors = typeof args.selectors != 'undefined' ? args.selectors : []; // google analytics measurament id

    this.gaid = typeof args.gaid != 'undefined' ? args.gaid : null; // Scroller needed to handle the event "element get in viewport"

    this._scroller = typeof args.scroller != 'undefined' ? args.scroller : new Scroller(); // array of elements to be tracked : [{ element, action, args }, ...]

    this._watch = []; // bind the context of _watchInit to the Tracker object.
    //this._watchInit.bind(this);

    this.init();
  }

  _createClass(Tracker, [{
    key: "init",
    value: function init() {
      var _this = this;

      // scan the selector list and initialize each tracker
      for (var i = 0; i < this.selectors.length; i++) {
        this._initSelector(this.selectors[i]);
      } // starts the view loop


      this._scroller.actionStack.push(function () {
        _this._watchInit();
      });
    }
  }, {
    key: "_watchInit",
    value: function _watchInit() {
      for (var i = 0; i < this._watch.length; i++) {
        // if the element get into the viewport
        if (this._scroller.isInViewPort(this._watch[i].element, 0)) {
          // tracks the event
          this.trackEvt(this._watch[i].action, this._watch[i].args); // removes the element from the watch queue

          this._watch.splice(i, 1);
        }
      }
    } // initialize a single selector

  }, {
    key: "_initSelector",
    value: function _initSelector(selector) {
      switch (selector.type) {
        case 'event':
          var elements = document.querySelectorAll(selector.selector);
          var args = typeof selector.args != 'undefined' ? selector.args : {
            'event_category': 'track'
          };

          for (var i = 0; i < elements.length; i++) {
            console.log('_initSelector');
            this.initClick(elements[i], selector.name, args);
            this.initView(elements[i], selector.name, args);
          }

          break;

        case 'pageview':
          break;
      }
    }
    /**
     * @method initView
     *
     */

  }, {
    key: "initView",
    value: function initView(element, name, args) {
      // check if already in the watch queue
      if (this._watch.indexOf(element) > -1) return;
      var action = name + ' view'; // if element is already in viewport

      if (this._scroller.isInViewPort(element, 0)) // track the view event
        this.trackEvt(action, args);else // add element to watch queue
        this._watch.push({
          element: element,
          action: action,
          args: args
        });
    }
    /**
     * @method initClick
     * prende tutti i selettori e attiva gli eventi per rilevare i click
     *
     * @param {*} element
     */

  }, {
    key: "initClick",
    value: function initClick(element, name, args) {
      var _this2 = this;

      var action = name + ' click';
      element.addEventListener('click', function () {
        return _this2.trackEvt(action, args);
      });
    }
    /**
     * @method add
     * 
     * @param {string} selector 
     * @param {'event'|'pageview'} type      
     * @param {string} name 
     * @param {object} args : { page_title : 'string', page_path : 'string', event_name : 'string', event_category : 'string'}     
     */

  }, {
    key: "add",
    value: function add(selector, type, name, args) {
      var _selector = {
        selector: selector,
        type: type,
        name: name
      };
      if (typeof args != 'undefined') _selector.args = args;

      this._initSelector(_selector);
    }
    /**
     * @method trackPV
     * 
     * @param {Object} args 
     * @param {*} GAID - google analytics measurament id
     */

  }, {
    key: "trackPV",
    value: function trackPV(args, GAID) {
      var gaid = typeof GAID != 'undefined' ? gaid = GAID : this.gaid;
      if (gaid == null) return;
      var title = null;
      var path = null; // path without the domain

      var currentpath = window.location.pathname + window.location.search + window.location.hash;

      if (args != null) {
        title = typeof args.page_title != 'undefined' ? args.page_title : document.title;
        path = typeof args.page_path != 'undefined' ? args.page_path : currentpath;
        gtag('config', gaid, {
          'page_title': title,
          'page_path': path
        });
      } else {
        gtag('config', gaid);
      }
    }
    /**
     * @method trackEvt
     *
     * @param {string} name { 
     * @param {Object} args { 
     *      'callback' : function
     *      'event_category': <category>,
     *      'event_label': <label>,
     *      'value': <value>
     * }
     * @param {*} GAID - google analytics measurament id
     */

  }, {
    key: "trackEvt",
    value: function trackEvt(name, args, GAID) {
      var gaid = typeof GAID != 'undefined' ? gaid = GAID : this.gaid;
      console.log('TRACK ', name, args);
      if (gaid == null) return;
      if (typeof name == 'undefined' || name == null || name == '') return;
      if (typeof args != 'undefined') gtag('event', name, args);else gtag('event', name);
    }
  }]);

  return Tracker;
}();
/*
https://developers.google.com/analytics/devguides/collection/gtagjs/events

gtag('event', <action>, {
  'event_category': <category>,
  'event_label': <label>,
  'value': <value>
});

<action> is the string that will appear as the event action in Google Analytics Event reports.
<category> is the string that will appear as the event category.
<label> is the string that will appear as the event label.
<value> is a non-negative integer that will appear as the event value.
*/
/*! npm.im/object-fit-images 3.2.4 */
var objectFitImages=function(){"use strict";function t(t,e){return"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='"+t+"' height='"+e+"'%3E%3C/svg%3E"}function e(t){if(t.srcset&&!p&&window.picturefill){var e=window.picturefill._;t[e.ns]&&t[e.ns].evaled||e.fillImg(t,{reselect:!0}),t[e.ns].curSrc||(t[e.ns].supported=!1,e.fillImg(t,{reselect:!0})),t.currentSrc=t[e.ns].curSrc||t.src}}function i(t){for(var e,i=getComputedStyle(t).fontFamily,r={};null!==(e=u.exec(i));)r[e[1]]=e[2];return r}function r(e,i,r){var n=t(i||1,r||0);b.call(e,"src")!==n&&h.call(e,"src",n)}function n(t,e){t.naturalWidth?e(t):setTimeout(n,100,t,e)}function c(t){var c=i(t),o=t[l];if(c["object-fit"]=c["object-fit"]||"fill",!o.img){if("fill"===c["object-fit"])return;if(!o.skipTest&&f&&!c["object-position"])return}if(!o.img){o.img=new Image(t.width,t.height),o.img.srcset=b.call(t,"data-ofi-srcset")||t.srcset,o.img.src=b.call(t,"data-ofi-src")||t.src,h.call(t,"data-ofi-src",t.src),t.srcset&&h.call(t,"data-ofi-srcset",t.srcset),r(t,t.naturalWidth||t.width,t.naturalHeight||t.height),t.srcset&&(t.srcset="");try{s(t)}catch(t){window.console&&console.warn("https://bit.ly/ofi-old-browser")}}e(o.img),t.style.backgroundImage='url("'+(o.img.currentSrc||o.img.src).replace(/"/g,'\\"')+'")',t.style.backgroundPosition=c["object-position"]||"center",t.style.backgroundRepeat="no-repeat",t.style.backgroundOrigin="content-box",/scale-down/.test(c["object-fit"])?n(o.img,function(){o.img.naturalWidth>t.width||o.img.naturalHeight>t.height?t.style.backgroundSize="contain":t.style.backgroundSize="auto"}):t.style.backgroundSize=c["object-fit"].replace("none","auto").replace("fill","100% 100%"),n(o.img,function(e){r(t,e.naturalWidth,e.naturalHeight)})}function s(t){var e={get:function(e){return t[l].img[e?e:"src"]},set:function(e,i){return t[l].img[i?i:"src"]=e,h.call(t,"data-ofi-"+i,e),c(t),e}};Object.defineProperty(t,"src",e),Object.defineProperty(t,"currentSrc",{get:function(){return e.get("currentSrc")}}),Object.defineProperty(t,"srcset",{get:function(){return e.get("srcset")},set:function(t){return e.set(t,"srcset")}})}function o(){function t(t,e){return t[l]&&t[l].img&&("src"===e||"srcset"===e)?t[l].img:t}d||(HTMLImageElement.prototype.getAttribute=function(e){return b.call(t(this,e),e)},HTMLImageElement.prototype.setAttribute=function(e,i){return h.call(t(this,e),e,String(i))})}function a(t,e){var i=!y&&!t;if(e=e||{},t=t||"img",d&&!e.skipTest||!m)return!1;"img"===t?t=document.getElementsByTagName("img"):"string"==typeof t?t=document.querySelectorAll(t):"length"in t||(t=[t]);for(var r=0;r<t.length;r++)t[r][l]=t[r][l]||{skipTest:e.skipTest},c(t[r]);i&&(document.body.addEventListener("load",function(t){"IMG"===t.target.tagName&&a(t.target,{skipTest:e.skipTest})},!0),y=!0,t="img"),e.watchMQ&&window.addEventListener("resize",a.bind(null,t,{skipTest:e.skipTest}))}var l="fregante:object-fit-images",u=/(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g,g="undefined"==typeof Image?{style:{"object-position":1}}:new Image,f="object-fit"in g.style,d="object-position"in g.style,m="background-size"in g.style,p="string"==typeof g.currentSrc,b=g.getAttribute,h=g.setAttribute,y=!1;return a.supportsObjectFit=f,a.supportsObjectPosition=d,o(),a}();

"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * main.js
 * 
 * responsabilità:
 * 
 * istanziare tutti i meccanismi di base della pagina
  
 * - inizializzazione dei worker(loader, tracciamenti)
 * - inizializzazione dell'infinite scroll (quando richiesto)
 * - inizializzazione di alcuni flag di controllo
 * 
 */
// import 'Page.js';
var Main = /*#__PURE__*/function () {
  function Main() {
    _classCallCheck(this, Main);

    // 1. reperimento delle caratteristiche della pagina
    this.page = new Page(); //console.log("page loaded", this.page.props);

    this.is_staging = window.location.hostname.indexOf('staging') > -1;
    this.is_static = window.location.hostname.indexOf('static') > -1;
    this.is_local = window.location.hostname.indexOf('localhost') > -1;
    this.script_version = this.getSearchString().ver;
    this.is_prod = !this.is_staging && !this.is_static && !this.is_local;
    this.adblock = this.isAdBlocked();
    this.isMobile = this.isMobile();
    this.ieVersion = this.ieVersion();
    this.no_ie_fallback = this.ieVersion > 10 || this.ieVersion == 0; // 2. caricamento asincrono delle dipendenze dei componenti       

    this.dynamicDeps = new DynamicDepsLoader({
      jsdir: this.is_local || this.is_static ? '/' : '/wp-content/themes/primascelta/',
      cssdir: this.is_local || this.is_static ? '/' : '/wp-content/themes/primascelta/',
      js_dep_selector: '[data-js-dependency]',
      css_dep_selector: '[data-css-dependency]',
      ie_fallback: !this.no_ie_fallback,
      version: this.script_version,
      nocache: false // only for debug

    }); // 3. Inizializzazione dello scroller

    this.scroller = new Scroller();
    console.log('scroller initialized');
    this.page.base_url = this.is_local || this.is_static ? '/' : '/wp-content/themes/newstreet/'; // 4. Inizializzazione del tracker

    this.tracker = new Tracker({
      gaid: 'UA-2021044-35',
      scroller: this.scroller
    });
    console.log('tracker initialized'); // 5. Inizializzazione dell'infinite scroll         

    /*this.infinite = (this.page.props.infinite == "true") ? new InfiniteScroll(this.page, {
        scroller : this.scroller,
        tracker: this.tracker
    }) : null;
     console.log('infinite initialized', this.page.props.infinite, this.infinite);
    
    
    if(this.infinite) {
        // 6. Actions to repeat for each infinite loading
        this.infinite.loader.loadCallbackStack.push(()=>{
            // check for new dependencies
            this.dynamicDeps.getDeps();
        });
        
        
        // 7. Set the callback to execute when maxLoads are reached            
        this.infinite.loader.onMaxLoads = function() {
            console.log('Maxloads Reached, to override this assigne a new function to:\nUI.infinite.loaded.onMaxLoads');
        }
    }
    
     console.log('infinite callback stack defined', this.no_ie_fallback)
    */
    // 7. actions and behaviours fore the whole site and the above the fold
    //this.actions = (typeof UIActions != 'undefined')? new UIActions(this.scroller) : null;

    this.actions = new UIActions({
      scroller: this.scroller
    });
    console.log('actions initialized', this.no_ie_fallback);
  }

  _createClass(Main, [{
    key: "isMobile",
    value: function isMobile() {
      var ismobile = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || screen.width <= 720 || window.innerWidth <= 720;
      return ismobile;
    }
  }, {
    key: "isAdBlocked",
    value: function isAdBlocked() {
      return false;
    }
  }, {
    key: "getSearchString",
    value: function getSearchString() {
      // get the querystring from the tag
      var string = document.querySelector('script[src*="main.js"]').src.split('?')[1];
      var obj = {}; // estract the key-values pairs into an object

      string.replace(/([^=&]+)=([^&]*)/g, function (m, key, value) {
        obj[decodeURIComponent(key)] = decodeURIComponent(value);
      });
      return obj;
    }
  }, {
    key: "ieVersion",
    value: function ieVersion(uaString) {
      var _uaString = uaString || navigator.userAgent;

      var match = /\b(MSIE |Trident.*?rv:|Edge\/)(\d+)/.exec(_uaString);
      if (match) return parseInt(match[2]);else return 0;
    }
  }]);

  return Main;
}();

var UI = new Main();
/*
var lazy = new Lazy({
    scroller : (typeof UI.scroller != 'undefined') ? UI.scroller : new Scroller(),
    isMobile : UI.isMobile,
    infinite : UI.infinite
});

console.log('blz - device is '+ ((UI.isMobile)?'':'NOT ')+'mobile');
console.log((UI.ieVersion>0 && UI.ieVersion<10)?'Needed polyfill' : 'No polyfill needed');
*/

function IE9Fallback(UI) {
  console.log('load polifills');
  var cssList = [];
  var jsList = ['polyfills/ofi.min.js', 'lib/smooth-scroll.polyfills.min.js', 'lib/polyfill.object-fit.min.js'];
  var baseCSSdir = UI.is_local || UI.is_static ? '/styles/' : '/wp-content/themes/newstreet/styles/';
  var baseJSdir = UI.is_local || UI.is_static ? '/scripts/' : '/wp-content/themes/newstreet/scripts/';

  for (var i = 0; i < cssList.length; i++) {
    document.head.insertAdjacentHTML('beforeend', '<link rel="stylesheet" type="text/css" href="' + baseCSSdir + cssList[i] + '">');
  }

  for (var i = 0; i < jsList.length; i++) {
    document.head.insertAdjacentHTML('beforeend', '<script src="' + baseJSdir + jsList[i] + '"></script>');
  }
}

if (UI.ieVersion > 0 && UI.ieVersion <= 10) IE9Fallback(UI);
/** CARICARE I POLYFILL PER IE9
// esempio di utilizzo
if(typeof UI != 'undefined') 

    const self = this;
    UI.infinite.loader.loadCallbackStack.push(function(){

        myFunction.call(self) // <-- occhio al contesto del this!
    });
}
*/