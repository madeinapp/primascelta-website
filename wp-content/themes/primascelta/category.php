<?php
/**
 * Template Name: Category Temeplate
 *
 */

get_header(); ?>


<div class="boxed hero-block" style="padding-bottom:0">
            <section class="heading">
				<h1>News</h1>
</section>
</div>
	
<div class="boxed intro">
    <?php
    $current_page = get_queried_object();
    $category     = $current_page->post_name;

    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    $query = new WP_Query( 
        array(
            'paged'         => $paged, 
            'category_name' => $category,
            'order_by'         => 'date',
            'order'         => 'desc',
            'post_type'     => 'post',
            'post_status'   => 'publish',
        )
    );

    if ($query->have_posts()) {
            while ($query->have_posts()) { 
            $query->the_post(); ?>

            
                <h5><a href="<?php echo get_permalink();?>" rel="bookmark"><?php echo get_the_title();?></a></h5>
                <span><?php the_excerpt(); ?></span>
                <div style="text-align:right"><i><time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time></i></div>
                <hr>  
            
            <?php
        }

        wp_reset_postdata();
    }
    ?>
    <div style="text-align:center"><?php  pagination_bar();?></div>
</div>
<?php get_footer(); 

function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
?>