<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
$contents = parse_blocks(get_the_content());
//pre($contents,true);
?>
                      
<div class="boxed hero-block" style="padding-bottom:0">
            <section class="heading">
				<h1><?php the_title(); ?></h1>
</section>
</div>
	<div class="boxed intro">
		<?php
		for ($i=0; $i<count($contents);$i++) {
			if ($contents[$i]["blockName"] == "core/separator"){
				$first = $i;
				break;
			} 
			echo $contents[$i]["innerHTML"];
		}
		?>
		<h2 class="evidence">
			Scegli l’abbonamento annuale e risparmi oltre il 25%!
		</h2>
		<section class="plans">
			<input type="checkbox" id="fatturazione" hidden checked/>
			<div class="fieldset">
				<label class="fat-sem" for="fatturazione">contratto semestrale</label>
				<div id="fatturazione-switch"></div>
				<label class="fat-ann" for="fatturazione">contratto annuale</label>
			</div>
			
			<div class="plans_container">
				
			<div class="plan plan--annuale">
			<div class="col">
					
					<div class="plans__card plans__card--standard">
						<header style="background-color: #4d64ee;">Basic</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/basic.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>1</span> SEDE ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>15</span> prodotti
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li style="text-decoration: line-through;text-decoration-color:red">INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							Gratuito 
						</div>
						
					</div>
					
				</div>

				<div class="col">
					
					<div class="plans__card plans__card--standard">
						<header>Standard</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/standard.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>1</span> SEDE ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>100</span> prodotti
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							15 <span>€ / mese</span>
						</div>
						<span>contratto annuale € 180 + iva</span>
					</div>
					
					
				</div>
				
				<div class="col">
					
					<div class="plans__card plans__card--premium">
						<header>Premium</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/premium.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>2</span> SEDI ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>200</span> prodotti per sede
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							24 <span>€ / mese</span>
						</div>
						<span>contratto annuale € 288 + iva</span>
					</div>
					
				
					
				</div>
				<div class="col">
					
					
					<div class="plans__card plans__card--enterprise">
						<header>Enterprise</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/enterprise.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>3</span> SEDI ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small><span>400</span> prodotti per sede
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							30 <span>€ / mese</span>
						</div>
						<span>contratto annuale € 360 + iva</span>
					</div>
					
					
				</div>
			</div>
			
			<div class="plan plan--semestrale">
			<div class="col">
					
					<div class="plans__card plans__card--standard">
						<header style="background-color: #4d64ee;">Basic</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/basic.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>1</span> SEDE ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>15</span> prodotti
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li style="text-decoration: line-through;text-decoration-color:red">INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							Gratuito 
						</div>
						
					</div>
					
				</div>
				<div class="col">
					
					<div class="plans__card plans__card--standard">
						<header>Standard</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/standard.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>1</span> SEDE ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>100</span> prodotti
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							19 <span>€ / mese</span>
						</div>
						<span>contratto semestrale € 114 + iva</span>
					</div>
					
				
				</div>
				
				<div class="col">
					
					<div class="plans__card plans__card--premium">
						<header>Premium</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/premium.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>2</span> SEDI ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>200</span> prodotti per sede
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							31 <span>€ / mese</span>
						</div>
						<span>contratto semestrale € 186 + iva</span>
					</div>
					
				
					
				</div>
				<div class="col">
					
					
					<div class="plans__card plans__card--enterprise">
						<header>Enterprise</header>
						<div class="plans__card__img">
							<img src="/wp-content/uploads/2020/04/enterprise.png" />
						</div>
						<div class="plan__data">
							<ul>
								<li>
									<p>
										<span>3</span> SEDE ATTIVITA’
									</p>
									<sup>1</sup>
								</li>
								<li>
									<p>
										<small>fino a </small> <span>400</span> prodotti per sede
									</p>
									<sup>2</sup>
								</li>
								<li>CATALOGO  PERSONALIZZATO <sup>3</sup></li>
								<li>INVIO OFFERTE SCONTATE <sup>4</sup></li>
								<li>AVVISI IN TEMPO REALE</li>
								<li>GESTIONE  PRENOTAZIONI</li>
								<li>FILTRO CLIENTI AFFIDABILI</li>
							</ul>
						</div>
						<div class="plan__price">
							38 <span>€ / mese</span>
						</div>
						<span>contratto semestrale € 228 + iva</span>
					</div>
					
					
				</div>
			</div>
		</div>
			
		<h2 class="evidence1" style="background-color:#c95c2a;border-radius: 20px;">
			<a href="/scaricaprimascelta/" style="color:#FFF;">SCARICA ORA PRIMASCELTA, PUOI PROVARLA GRATIS ... PER SEMPRE</a>
		</h2>
			
		
			
	</section>
		
		<section class="rules">
			<div class="boxed">
			<?php
			for ($i=$first; $i<count($contents);$i++) {
				echo $contents[$i]["innerHTML"];
			}
			?>
			</div>
			
		</section>
		
	</div>

	
	<?php endwhile; endif; ?>
                
<?php get_footer(); ?>
