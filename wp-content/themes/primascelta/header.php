<!DOCTYPE html>
<html class="theme-light" <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8" />
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
    

    <meta name="author" content="MadeInApp" />
    <meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no, viewport-fit=cover, maximum-scale=5" />
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/primascelta/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/primascelta/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/primascelta/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/primascelta/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="/wp-content/themes/primascelta/assets/favicon/safari-pinned-tab.svg" color="#252637">
    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="theme-color" content="#ffffff">
    <script>
      /*
        SOLO PER DEBUG
      */
      //window.localStorage.clear();
      function clearLocalStorage(){
        var ids = Object.keys(localStorage);
        for(var i=0;i<ids.length;i++) if(ids[i].indexOf('basket') > -1 )localStorage.removeItem(ids[i])
      }

      clearLocalStorage();
    </script>


    <style>#blz-copy-to-clipboard{position:absolute;top:0;left:-6000px!important}.search-model.desktop{display:flex;padding-top:1px}.search-model.desktop select{text-transform:none;height:30px;margin:0 24px 0 0;border:0;background:#eeeef0;color:#ccc;padding:0 0 0 8px;border-radius:4px;min-width:218px}.search-model.desktop button{min-width:58px;min-height:30px;max-height:30px;line-height:1;background-color:#dfe3ed}.btn.default{background-color:#dfe3ed;background:#dfe3ed}.card.card-brand img{max-width:65px;margin:0 auto 8px}.card.card-brand{text-align:center;margin:12px 0}a.toggle-content{margin:auto;display:block;font-weight:600}.lnd-image{min-width:148px;max-width:148px;min-height:134px}.fixed-width-filters{width:300px;opacity:0;transition:opacity .2s ease}.listino .mobile-handle{display:none}section.listato-marche{height:264px;display:block;opacity:0;transition:opacity .5s ease-out}@media only screen and (max-width:780px){section.listato-marche{opacity:1}}.scheda-info.desktop{position:absolute}.scheda-info{position:absolute;display:block;box-sizing:border-box;padding:16px;padding-left:0;width:100%;align-items:center;bottom:0;left:0;color:#fff}.scheda-info .brand-content{display:flex;align-content:center;justify-content:left}.scheda-info .brand-logo{width:105px;margin:0 16px}ol.breadcrumb{padding:12px 0;margin:0;list-style-type:none}ol.breadcrumb li{display:inline}.single-excerpt.mobile-excerpt{display:none}</style>
    <!--[if IE 9]>
      <style>
      .main-container{padding-top: 12px !important}.grid {display:block;}.col{display:block; float:left; clear:right; width: 50%;}section.listato-marche{opacity:1;text-align: center;}.desktop.search-model{display:none}section.vetrina-widget{background: white!important}.menu{background: transparent !important;}.menu-icon{top: 0px !important; right: 0!important; position: absolute !important}.search-button{top: 14px; right: 50px; position: absolute;}.table-layout .widget.article-main{padding: 0 42px 0 24px !important}.navbar{position: relative!important}section.vetrina-widget{display: inline-block; width: 100%}.grid-sh > div{display: table-cell; vertical-align: top; height: 150px}.card-square-featured{height: 322px}.toggle-content{line-height: 112px;}.footer-nav{width: 100%; margin-top: 24px; display: inline-block;}section.vetrina-widget .section-title span{color: #272838!important}.card-photo{width: 300px; height: 170px; display: table-cell; vertical-align: top; background-repeat: no-repeat; background-position-x: center; background-size: 100%;color:white; text-align: center;}.swiper-wrapper{   display: block; overflow-x: scroll; overflow-y: hidden; white-space: nowrap; padding-bottom: 0; margin: 0;}.swiper-slide{display:inline-block;}.card{font-weight: 900}.grey{color: #cacaca}.cardlink{display:block}.swiper-container.ft-carousel .swiper-wrapper{height: 320px}.swiper-container.ft-carousel .swiper-wrapper .card{height: 320px}.scheda-info.desktop{display:none!important}.mobile-excerpt.single-excerpt{display:block;}.share-container ul{list-style-type: none; padding-left: 22px; width:32px;}.share-container ul li{width:32px}.share-container ul li a{color: white; display:block; width:32px; height:32px; text-align: center; line-height: 34px}.social-button.social.fb{background: #ed1a4d}.social-button.social.twitter{background: #0f3591}.social-button.social.linkedin{background: #66a9d7}.social.copy{background: #272838}.mobile-post-info{display:none}.tag-section ul{list-style-type: none; padding-left: 0;}.tag-section ul li{display: inline-block; margin: 4px; background: #dfe3ed; padding: 2px 8px 4px; border-radius: 4px;}.share-container.horizontal{display: none}
      </style>
    <![endif]-->
<?php /** 
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-WPZ3XHG');</script>
*/?>


    <link rel="stylesheet" type="text/css" href="/wp-content/themes/primascelta/styles/core/core.css?v=1.0" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap" rel="stylesheet">

    <title><?php wp_title(''); ?></title>

    <?php wp_head(); ?>  

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3M34V6YKBL"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-3M34V6YKBL');
    </script>
      
      </head>

  <body  <?php body_class(); ?>>


        <?php // wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
      <div id="menu-panel" class="menu-panel">
		  <div id="menu-close" onclick="closeMenu()">
			  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" stroke-width="2"><g stroke-width="2" transform="translate(0, 0)"><line fill="none" stroke="#ffffff" stroke-width="4" stroke-linecap="square" stroke-miterlimit="10" x1="32" y1="16" x2="16" y2="32" stroke-linejoin="miter"></line> <line fill="none" stroke="#ffffff" stroke-width="4" stroke-linecap="square" stroke-miterlimit="10" x1="32" y1="32" x2="16" y2="16" stroke-linejoin="miter"></line></g></svg>
		  </div>

				<div class="menu-container">
					<div class="grid">
          <?php foreach (wp_get_nav_menu_items('MENU TOP') as $voceMenu) : ?>
          <div class="col col-4 col-sm"><a href="<?=$voceMenu->url ?>"><span class="menutitle magazine"><?=$voceMenu->title?></span></a></div>
          <?php endforeach; ?>
					
					</div>
					<div class="grid" style="flex-direction: column">
						<a href="/scaricaprimascelta/" class="btn btn_orange ">INIZIA LA TUA PROVA GRATUITA</a><br>
				  		<a href="https://backend.primascelta.biz/login" target="blank"  class="btn btn_grey">ACCEDI</a>
					</div>
				</div>
			</div>
	  
	  
    <div class="main-wrapper">
            <header class="main-header">
    <div class="navbar">
      <div class="navbar-wrapper">
          <div class="navbar-container">
              <div class="logo">
               <a href="/" class="logotext"><img src="/wp-content/uploads/2020/04/logo-primascelta.png" alt="logo Primafila"/></a>
              </div>
			  <div class="navbar-container__menu">
				  <ul>
          <?php foreach (wp_get_nav_menu_items('MENU TOP') as $voceMenu) : ?>
          <li><a href="<?=$voceMenu->url ?>" title="<?=$voceMenu->title?>"><?=$voceMenu->title?></a></li>
          <?php endforeach; ?>
				  </ul>
			  </div>
			  <div class="navbar-container__right">
				  <a href="/scaricaprimascelta/" class="btn btn_orange ">INIZIA LA TUA PROVA GRATUITA</a>
				  <a href="https://backend.primascelta.biz/login" target="blank" class="btn btn_grey">ACCEDI</a>
				  <span class="hamburger" onclick="openMenu()"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 12 12"><g transform="translate(0, 0)"><path d="M11,9H1a1,1,0,0,0,0,2H11a1,1,0,0,0,0-2Z" fill="#fff"></path> <path d="M11,1H1A1,1,0,0,0,1,3H11a1,1,0,0,0,0-2Z" fill="#fff"></path> <path d="M11,5H1A1,1,0,0,0,1,7H11a1,1,0,0,0,0-2Z" fill="#fff" data-color="color-2"></path></g></svg>
				  </span>
			  </div>
          </div>
      </div>
    </div>
</header>       
<div class="main-container" data-js-dependency="scripts/polyfills/classlist-polyfill.js,scripts/components/cookieConsent.js, scripts/components/cookieConsent-settings.js" data-css-dependency="styles/components/partials/cookieConsent.css">
      <div id="pageTop"></div>