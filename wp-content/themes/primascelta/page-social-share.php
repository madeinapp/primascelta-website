<html class="theme-light" lang="it-IT"><head><style>@keyframes beginBrowserAutofill{0%{}to{}}@keyframes endBrowserAutofill{0%{}to{}}.pac-container{background-color:#fff;position:absolute!important;z-index:1000;border-radius:2px;border-top:1px solid #d9d9d9;font-family:Arial,sans-serif;box-shadow:0 2px 6px rgba(0,0,0,0.3);-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden}.pac-logo:after{content:"";padding:1px 1px 1px 0;height:18px;box-sizing:border-box;text-align:right;display:block;background-image:url(https://maps.gstatic.com/mapfiles/api-3/images/powered-by-google-on-white3.png);background-position:right;background-repeat:no-repeat;background-size:120px 14px}.hdpi.pac-logo:after{background-image:url(https://maps.gstatic.com/mapfiles/api-3/images/powered-by-google-on-white3_hdpi.png)}.pac-item{cursor:default;padding:0 4px;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;line-height:30px;text-align:left;border-top:1px solid #e6e6e6;font-size:11px;color:#999}.pac-item:hover{background-color:#fafafa}.pac-item-selected,.pac-item-selected:hover{background-color:#ebf2fe}.pac-matched{font-weight:700}.pac-item-query{font-size:13px;padding-right:3px;color:#000}.pac-icon{width:15px;height:20px;margin-right:7px;margin-top:6px;display:inline-block;vertical-align:top;background-image:url(https://maps.gstatic.com/mapfiles/api-3/images/autocomplete-icons.png);background-size:34px}.hdpi .pac-icon{background-image:url(https://maps.gstatic.com/mapfiles/api-3/images/autocomplete-icons_hdpi.png)}.pac-icon-search{background-position:-1px -1px}.pac-item-selected .pac-icon-search{background-position:-18px -1px}.pac-icon-marker{background-position:-1px -161px}.pac-item-selected .pac-icon-marker{background-position:-18px -161px}.pac-placeholder{color:gray}.pac-target-input:-webkit-autofill{animation-name:beginBrowserAutofill}.pac-target-input:not(:-webkit-autofill){animation-name:endBrowserAutofill}
</style>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="author" content="MadeInApp">
    <meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no, viewport-fit=cover, maximum-scale=5">
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/primascelta/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/primascelta/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/primascelta/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/primascelta/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="/wp-content/themes/primascelta/assets/favicon/safari-pinned-tab.svg" color="#252637">
    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="theme-color" content="#ffffff">
    
	<?php
	$title = "Primascelta: L'app per la prenotazione e la consegna cibo";
	$shortDescr = "Sei un privato in cerca del miglior cibo da mettere in tavola, scarica Primascelta: per te è gratis!";
	$descr = $shortDescr;
	$image = "<p><br>";
	$imageOG = "http://www.primascelta.biz/wp-content/uploads/2020/04/hero-image.png";
	$URL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if (isset($_GET["shop"])){
		$shortDescr = "Se sei un'azienda del settore food, Primascelta è la soluzione più completa ed economica per gestire in modo semplice le prenotazioni dei tuoi prodotti e farti trovare da nuovi clienti. Scarica la nostra App senza costi nascosti e a canone fisso!";
		$descr = "Se sei un'azienda del settore food, Primascelta è la soluzione più completa ed economica per gestire in modo semplice le prenotazioni dei tuoi prodotti e farti trovare da nuovi clienti. <br><br>Scarica la nostra App senza costi nascosti e a canone fisso!";
	}
	if (isset($_GET["idshop"])){
		$call = json_decode(getSSLPage('https://backend.primascelta.biz/share/shop/'.$_GET["idshop"]));
		$shortDescr = "Scopri le migliori offerte e i prodotti di ".$call->success->name." sull'app primascelta. Scaricala, per te è gratis!";
		$descr = "Scopri le migliori offerte e i prodotti di &nbsp;<b>".$call->success->name."</b> sull'app primascelta. Scaricala, per te è gratis!";
		$image = "<img src=\"".$call->success->image."\" class=\"img-responsive\"><p><br>";
		$imageOG = $call->success->image;
	}
	if (isset($_GET["shop_catalog"])){
		$call = json_decode(getSSLPage('https://backend.primascelta.biz/share/shop_catalog/'.$_GET["shop_catalog"]));
		$shortDescr = $call->success->name . " a " . $call->success->price. " in vendita da ". $call->success->shop_name." sull'app primascelta. Scaricala, per te è gratis!";
		$descr = "<b>".$call->success->name . "</b> a <i>" . $call->success->price. "</i> in vendita da <b>". $call->success->shop_name."</b> sull'app primascelta. Scaricala, per te è gratis!";
		$descr = "<table><tr><td width=\"30%\"><img src=\"".$call->success->image."\" width=\"90%\" align=\"center\"></td><td width=\"70%\" style=\"font-size: 14px;\"><b>".$call->success->name . "</b> a <i>" . $call->success->price. "</i></td></tr></table><br>In vendita da <b>". $call->success->shop_name."</b> sull'app primascelta. Scaricala, per te è gratis!";
		$imageOG = $call->success->image;
	}
	if (isset($_GET["shop_offer"])){
		$call = json_decode(getSSLPage('https://backend.primascelta.biz/share/shop_offer/'.$_GET["shop_offer"]));
		//print_r($call);exit;
		if ($call->success->tot_prodotti == 1){
			$shortDescr = $call->success->prodotti[0]->name . " a " . $call->success->prodotti[0]->discounted_price. " invece di " .$call->success->prodotti[0]->original_price. " in vendita da ". $call->success->shop_name." sull'app primascelta. Scaricala, per te è gratis!";
			$descr = "<table><tr><td width=\"30%\"><img src=\"".$call->success->prodotti[0]->image."\" width=\"90%\" align=\"center\"></td><td width=\"70%\" style=\"font-size: 14px;\"><b>".$call->success->prodotti[0]->name . "</b> a <i>" . $call->success->prodotti[0]->discounted_price. "</i> invece di <strike>" .$call->success->prodotti[0]->original_price."</strike></td></tr></table><br>In vendita da <b>". $call->success->shop_name."</b> sull'app primascelta. Scaricala, per te è gratis!";
			$imageOG = $call->success->image;
		}else{
			$shortDescr = $call->success->prodotti[0]->name . " a " . $call->success->prodotti[0]->discounted_price. " invece di " .$call->success->prodotti[0]->original_price. " in vendita da ". $call->success->shop_name." sull'app primascelta. Scaricala, per te è gratis!";
			$descr = "<table>";
			foreach($call->success->prodotti as $prodotto) 
				$descr .= "<tr><td width=\"30%\"><img src=\"".$prodotto->image."\" width=\"90%\" align=\"center\"></td><td width=\"70%\" style=\"font-size: 14px;\"><b>".$prodotto->name . "</b> a <i>" . $prodotto->discounted_price. "</i> invece di <strike>" .$prodotto->original_price."</strike></td></tr>";
			
			$descr .= "</table><br>in vendita da <b>". $call->success->shop_name."</b> sull'app primascelta. Scaricala, per te è gratis!";
			$imageOG = $call->success->image;
		}
		
	}
	if (isset($_GET["shop_offer_food"])){
		$call = json_decode(getSSLPage('https://backend.primascelta.biz/share/shop_offer_food/'.$_GET["shop_offer_food"]));
		//print_r($call);exit;
		$shortDescr = $call->success->name . " a " . $call->success->discounted_price. " invece di " .$call->success->original_price. " in vendita da ". $call->success->shop_name." sull'app primascelta. Scaricala, per te è gratis!";
		$descr = "<table><tr><td width=\"30%\"><img src=\"".$call->success->image."\" width=\"90%\" align=\"center\"></td><td width=\"70%\" style=\"font-size: 14px;\"><b>".$call->success->name . "</b> a <i>" . $call->success->discounted_price. "</i> invece di <strike>" .$call->success->original_price."</strike></td></tr></table><br>In vendita da <b>". $call->success->shop_name."</b> sull'app primascelta. Scaricala, per te è gratis!";
		$imageOG = $call->success->image;
	}
	?>


    <style>#blz-copy-to-clipboard{position:absolute;top:0;left:-6000px!important}.search-model.desktop{display:flex;padding-top:1px}.search-model.desktop select{text-transform:none;height:30px;margin:0 24px 0 0;border:0;background:#eeeef0;color:#ccc;padding:0 0 0 8px;border-radius:4px;min-width:218px}.search-model.desktop button{min-width:58px;min-height:30px;max-height:30px;line-height:1;background-color:#dfe3ed}.btn.default{background-color:#dfe3ed;background:#dfe3ed}.card.card-brand img{max-width:65px;margin:0 auto 8px}.card.card-brand{text-align:center;margin:12px 0}a.toggle-content{margin:auto;display:block;font-weight:600}.lnd-image{min-width:148px;max-width:148px;min-height:134px}.fixed-width-filters{width:300px;opacity:0;transition:opacity .2s ease}.listino .mobile-handle{display:none}section.listato-marche{height:264px;display:block;opacity:0;transition:opacity .5s ease-out}@media only screen and (max-width:780px){section.listato-marche{opacity:1}}.scheda-info.desktop{position:absolute}.scheda-info{position:absolute;display:block;box-sizing:border-box;padding:16px;padding-left:0;width:100%;align-items:center;bottom:0;left:0;color:#fff}.scheda-info .brand-content{display:flex;align-content:center;justify-content:left}.scheda-info .brand-logo{width:105px;margin:0 16px}ol.breadcrumb{padding:12px 0;margin:0;list-style-type:none}ol.breadcrumb li{display:inline}.single-excerpt.mobile-excerpt{display:none}</style>
    <!--[if IE 9]>
      <style>
      .main-container{padding-top: 12px !important}.grid {display:block;}.col{display:block; float:left; clear:right; width: 50%;}section.listato-marche{opacity:1;text-align: center;}.desktop.search-model{display:none}section.vetrina-widget{background: white!important}.menu{background: transparent !important;}.menu-icon{top: 0px !important; right: 0!important; position: absolute !important}.search-button{top: 14px; right: 50px; position: absolute;}.table-layout .widget.article-main{padding: 0 42px 0 24px !important}.navbar{position: relative!important}section.vetrina-widget{display: inline-block; width: 100%}.grid-sh > div{display: table-cell; vertical-align: top; height: 150px}.card-square-featured{height: 322px}.toggle-content{line-height: 112px;}.footer-nav{width: 100%; margin-top: 24px; display: inline-block;}section.vetrina-widget .section-title span{color: #272838!important}.card-photo{width: 300px; height: 170px; display: table-cell; vertical-align: top; background-repeat: no-repeat; background-position-x: center; background-size: 100%;color:white; text-align: center;}.swiper-wrapper{   display: block; overflow-x: scroll; overflow-y: hidden; white-space: nowrap; padding-bottom: 0; margin: 0;}.swiper-slide{display:inline-block;}.card{font-weight: 900}.grey{color: #cacaca}.cardlink{display:block}.swiper-container.ft-carousel .swiper-wrapper{height: 320px}.swiper-container.ft-carousel .swiper-wrapper .card{height: 320px}.scheda-info.desktop{display:none!important}.mobile-excerpt.single-excerpt{display:block;}.share-container ul{list-style-type: none; padding-left: 22px; width:32px;}.share-container ul li{width:32px}.share-container ul li a{color: white; display:block; width:32px; height:32px; text-align: center; line-height: 34px}.social-button.social.fb{background: #ed1a4d}.social-button.social.twitter{background: #0f3591}.social-button.social.linkedin{background: #66a9d7}.social.copy{background: #272838}.mobile-post-info{display:none}.tag-section ul{list-style-type: none; padding-left: 0;}.tag-section ul li{display: inline-block; margin: 4px; background: #dfe3ed; padding: 2px 8px 4px; border-radius: 4px;}.share-container.horizontal{display: none}
      </style>
    <![endif]-->


    <link rel="stylesheet" type="text/css" href="/wp-content/themes/primascelta/styles/core/core.css?v=1.0">
    <link href="<?php echo $URL;?>" rel="stylesheet">

    <title><?php echo $title;?></title>
    <meta name="description" content="<?php echo $shortDescr;?>">
	<meta property="fb:app_id"  content="283167932865863" >
	<meta property="og:locale" content="it_IT">
	<meta property="og:type" content="website">
	<meta property="og:title" content="<?php echo $title;?>">
	<meta property="og:description" content="<?php echo $shortDescr;?>">
	<meta property="og:image" content="<?php echo $imageOG;?>">
	<meta property="og:url" content="<?php echo $URL;?>">
	<meta property="og:site_name" content="Primascelta">
	<meta name="twitter:card" content="summary_large_image">
	<link rel="canonical" href="<?php echo $URL;?>" />
	
	<!-- / Yoast SEO plugin. -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=G-3M34V6YKBL"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-3M34V6YKBL');
    </script>
      
      </head>

  <body>
 
    <div class="main-wrapper">
            <header class="main-header">
    <div class="navbar scrolled">
      <div class="navbar-wrapper">
          <div class="navbar-container">
              <div class="logo">
               <a href="/" class="logotext"><img src="/wp-content/uploads/2020/04/logo-primascelta.png" alt="logo Primafila"></a>
              </div>
          </div>
      </div>
    </div>
</header>       
<div class="main-container">
             <div class="boxed hero-block" style="padding-bottom:0">
            <section class="heading">
                <h3 style="margin-left:10px">Se, in fatto di cibo, ti accontenti sempre.<br> Del meglio.</h3>
                <div class="grid">
                    <div class="hero-block__text col col-6 col-sm" style="background:rgba(244,242,242,0.9);border-radius: 20px; padding: 20px; margin-bottom: 20px;">
						<div class="grid" >
						<?php echo $image;?>
						<?php echo $descr;?>
						</div>
						<div class="grid">
							<div style="width:50%;text-align:center">
								<a href="javascript:void(0)" class="ctabtn" rel="nofollow"><img  width="140px" src="/wp-content/uploads/2020/04/appstore.png"></a>
							</div>
							<div  style="width:50%;text-align:center">
								<a href="javascript:void(0)" class="ctabtn" rel="nofollow"><img width="140px" src="/wp-content/uploads/2020/04/googleplay.png"></a>
							</div>
						</div>
                    </div>
                    <div class="hero-block__image col col-8 col-sm" style="margin-left:auto">
                        <div class="hero-image" >
                            <img src="/wp-content/uploads/2020/04/hero-image.png" class="img-responsive" alt="hero Primascelta">
                        </div>
                    </div>
                </div>
            </section>
        </div>
		
		    </div> <!-- main container -->            
        </div> <!-- main wrapper -->
</body></html>