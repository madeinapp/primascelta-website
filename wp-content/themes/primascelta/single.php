<?php get_header(); ?>
        <div class="boxed hero-block" style="padding-bottom:0">
            <section class="heading">
                <h1><?php the_title(); ?></h1>
                <div class="grid">
                    <div class=" col col-12 col-sm">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php if ( has_post_thumbnail() ) : ?>
                        <a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false ); echo esc_url( $src[0] ); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                   
                </div>
            </section>
        </div>
<?php get_footer(); ?>
