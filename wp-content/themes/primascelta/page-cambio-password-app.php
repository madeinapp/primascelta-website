<?php get_header(); 
$video = get_field("video");
if ($video != "") :?>
        <section class="faq hero-section">
            <div class="boxed flex">
                <div class="faq__bgimage faq__bgimage--1">
                    <img src="/wp-content/uploads/2020/04/FAQ-bg-sx.png"/>
                </div>
                <div class="video-container">
                    <div>
                    <iframe title="vimeo-player" src="<?php echo get_field("video");?>" width="740" height="416.25" frameborder="0" allowfullscreen ></iframe>
                    </div>
                </div>
                <div class="faq__bgimage faq__bgimage--2">
                    <img src="/wp-content/uploads/2020/04/FAQ-bg-dx.png"/>
                </div>
            </div>
        </section>
<?php endif;?>
        <div class="boxed hero-block " style="padding-bottom:0">
            <section  <?php echo ($video == "") ? 'class="heading pages " ' : ''?> >
            
                <h1 <?php echo ($video != "") ? 'class="FAQ-heading" style="margin-bottom:40px" ' : '' ?> >
                <?php the_title(); ?>
                </h1>
                <div class="grid">
                    <div class=" col col-12 col-sm">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php if ( has_post_thumbnail() ) : ?>
                        <a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false ); echo esc_url( $src[0] ); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <?php endwhile; endif; ?>

						<form id="modulo_password" action="https://backend.primascelta.biz/api/v1/auth/password-change" method="post">
						<input type="hidden" name="email" value="<?php echo $_GET["email"]?>">
						<input type="hidden" name="otp" value="<?php echo $_GET["otpcode"]?>">
						Password: <input type="password" name="password" value="" >
						Conferma Password: <input type="password" name="confermapassword" value="" >
                        <input type="button" onclick="validate()" value="invia nuova password">
						</form>
                    </div>
                   
                </div>
            </section>
        </div>

        <script>
        // controllo sulla validità della password
        var ck_password = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@()$%^&*=_{}[\]:;\"'|\\<>,.\/~`±§+-]).{8,30}$/;

         // array degli errori
         var errors = [];

        function validate(form) {
			var form = document.getElementById("modulo_password");
            errors = [];
            // definizione delle variabili
            var password = form.password.value;
            var confermapassword = form.confermapassword.value;

            // controllo sugli input
            if (!ck_password.test(password)) {
                errors[errors.length] = "La password deve essere di almeno 8 caratteri e includere maiuscole, minuscole, numeri e simboli.";
            }

            if (confermapassword != password) {
                errors[errors.length] = "Le password non coincidono.";
            }

            // controllo sulla presenza di errori
            if (errors.length > 0) {
                reportErrors(errors);
                return false;
            }


			var formData = new FormData();

			formData.append("email", form.email.value);
			formData.append("otp", form.otp.value); 
			formData.append("password", form.password.value); 

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://backend.primascelta.biz/api/v1/auth/password-change");
			xhr.send(formData);

			xhr.onreadystatechange = function() { // Call a function when the state changes.
				if (this.readyState === XMLHttpRequest.DONE) {
					if (this.status === 200) {
						self.location.href="https://www.primascelta.biz/password-ok/";
					} else {
						alert("Non è stato possibile cambiare la password, riprovare più tardi");
					}
				}
			}
        }

        // conteggio degli errori
        function reportErrors(errors){
        var msg = "Sono stati rilevati i seguenti errori:\n";
        for (var i = 0; i<errors.length; i++) {
            var numError = i + 1;
            msg += "\n" + numError + ". " + errors[i];
        }
        // notifiche di errore
        alert(msg);
        }
        </script>
<?php get_footer(); ?>
